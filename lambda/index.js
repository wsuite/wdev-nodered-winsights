var app = require("../core/app");
const wlog = require("wsuite-logger");
wlog.init();
const creator = require("../core/queryCreator");
const mitrabajoCountQuery = require("../core/mitrabajoCountQuery");
const countQuery = require("../core/countQuery");
const myTasksQuery = require("../core/myTasksQuery");
const myChartQuery = require("../core/myChartQuery");
const genericQuery = require("../core/genericQueryCreator");
const mySqlUtility = require("../core/mysql-utility/mysql-utility");
const qs = require("qs");


exports.handler = async event => {
	console.log("Request e {0}".format(JSON.stringify(event, null, 4)));
	const execution = event.Execution.Name;
	var configure = event.Config;
	const serviceConfig = event.serviceConfig;
	var config = configure[execution];
	var headers = event.headers;
	if (config.useCompanyDb) {
		event.Config[execution].dbhost = serviceConfig.details.host;
		event.Config[execution].dbname = serviceConfig.details.db;
		event.Config[execution].dbusername = serviceConfig.details.username;
		event.Config[execution].dbpassword = serviceConfig.details.password;
	}
	let query = creator(event, config, headers);
	if (["get", "delete"].indexOf(headers.httpmethod.toLowerCase()) == -1) {
		let data_query = event.Payload ? qs.stringify(event.Payload) : qs.stringify(event.payload);
		event.headers.query = data_query;
	}
	event.Payload ? (event.Payload.query = query) : (event.payload.query = query);
	const App = new app(config.accessKeyId, config.secretAccessKey, config.database);
	try {
		if (config.isathena == "" || config.isathena == false) {
			const response = {};
			switch (config.name) {
				case "mitrabajochart":
					try {
						const connection = mySqlUtility.dbConnection(config);
						const data = await mySqlUtility.getRecords(connection, query);
						if (data.error) {
							return Promise.reject(data);
						} else {
							const countQ = countQuery(query);
							const count = await mySqlUtility.getRecords(connection, countQ);
							if (count.error) {
								return Promise.reject(count);
							} else {
								response["data"] = data;
								response["count"] = count[0] ? count[0].total_records : 0;
							}
							connection.destroy();
							return Promise.resolve(response);
						}
					} catch (error) {
						return Promise.reject(errorStringify(error));
					}
					break;

				case "mitrabajocount":
					try {
						query = mitrabajoCountQuery(config, headers);
						event.Payload ? (event.Payload.query = query) : (event.payload.query = query);
						const connection = mySqlUtility.dbConnection(config);
						const data = await mySqlUtility.getRecords(connection, query);

						if (data.error) {
							return Promise.reject(data);
						}
						connection.destroy();
						response["data"] = data;
						response["count"] = 0;
						return Promise.resolve(response);
					} catch (error) {
						console.log("error ====>", error)
						return Promise.reject(errorStringify(error));
					}
					break;

				case "mytasks":
					try {
						query = myTasksQuery(headers);
						const connection = mySqlUtility.dbConnection(config);
						const data = await mySqlUtility.getRecords(connection, query);
						if (data.error) {
							return Promise.reject(data);
						} else {
							const countQ = countQuery(query);
							const count = await mySqlUtility.getRecords(connection, countQ);
							if (count.error) {
								return Promise.reject(count);
							} else {
								response["data"] = data;
								response["count"] = count[0] ? count[0].total_records : 0;
							}
							connection.destroy();
							return Promise.resolve(response);
						}
					} catch (error) {
						return Promise.reject(errorStringify(error));
					}
					break;

				case "mychart":
					try {
						query = myChartQuery(headers);
						const connection = mySqlUtility.dbConnection(config);
						const data = await mySqlUtility.getRecords(connection, query);
						if (data.error) {
							return Promise.reject(data);
						} else {
							const countQ = countQuery(query);
							const count = await mySqlUtility.getRecords(connection, countQ);
							if (count.error) {
								return Promise.reject(count);
							} else {
								response["data"] = data;
								response["count"] = count[0] ? count[0].total_records : 0;
							}
							connection.destroy();
							return Promise.resolve(response);
						}
					} catch (error) {
						return Promise.reject(errorStringify(error));
					}
					break;
				case "readGenericTable":
					try {
						query = genericQuery(config, headers, 'read');
						const connection = mySqlUtility.dbConnection(config);
						const data = await mySqlUtility.getRecords(connection, query);
						if (data.error) {
							return Promise.reject(data);
						}
						connection.destroy();
						response["data"] = data;
						response["count"] = data.length;
						return Promise.resolve(response);
					} catch (error) {
						console.log("error", error);
						return Promise.reject(errorStringify(error));
					}
					break;
				case "updateGenericTable":
					try {
						query = genericQuery(config, headers, 'update');
						const connection = mySqlUtility.dbConnection(config);
						const data = await mySqlUtility.getRecords(connection, query);
						console.log("here is the output", data)
						if (data.error) {
							return Promise.reject(data);
						}
						connection.destroy();
						response["data"] = data;
						response["count"] = data.length;
						return Promise.resolve(response);
					} catch (error) {
						console.log("error", error);
						return Promise.reject(errorStringify(error));
					}
					break;
				case "writeGenericTable":
					try {
						query = genericQuery(config, headers, 'write');
						const connection = mySqlUtility.dbConnection(config);
						const data = await mySqlUtility.getRecords(connection, query);
						if (data.error) {
							return Promise.reject(data);
						}
						connection.destroy();
						response["data"] = data;
						response["count"] = data.length;
						return Promise.resolve(response);
					} catch (error) {
						console.log("error", error);
						return Promise.reject(errorStringify(error));
					}
					break;
				case "deleteGenericTable":
					try {
						query = genericQuery(config, headers, 'delete');
						const connection = mySqlUtility.dbConnection(config);
						const data = await mySqlUtility.getRecords(connection, query);
						if (data.error) {
							return Promise.reject(data);
						}
						connection.destroy();
						response["data"] = data;
						response["count"] = data.length;
						return Promise.resolve(response);
					} catch (error) {
						console.log("error", error);
						return Promise.reject(errorStringify(error));
					}
					break;
				default:
					return Promise.reject(`Not found ${config.name} node`);
					break;
			}
		} else {
			const res = await App.getTableData(event);
			if (res.error) {
				return Promise.reject(res);
			} else {
				return Promise.resolve(res);
			}
		}
	} catch (error) {
		console.log("getTableData ---->", error);
		return Promise.reject(errorStringify(error));
	}
};

function errorStringify(error) {
	if(typeof error == "object") {
		return JSON.stringify(error);
	}
	return error;
}
