var customRoute = require('../app/route')
const uuidv1 = require('uuid/v1')
var AWS = require('aws-sdk')
var s3 = new AWS.S3()
var fs = require('fs')

exports.handler = async(e) => {
    console.log('======== indexv2 ============');
    console.log('e ----->', e);
    const execution = e.Execution.Name;
    const configArr = e.Config;
    const config = configArr[execution];
    const brandConfig = configArr['brandInfo'] || {};
    const headers = e.headers;
    const queryParam = (headers.query) ? headers.query : {};
    const payload = (headers.httpmethod === 'get') ? headers.query : e.Payload;

    console.log("headers ====> ", headers);
    console.log("queryParam ====> ", queryParam);

    var event = {
        payload: {requestData: payload},
        config: config,
        brand_config: brandConfig,
        queryParam: queryParam,
        headers: headers
    };
    console.log("event", event);
    var s3Params = {
        appId: event.headers.params.split("/")[0],
        s3Bucket: 'development-static.wsuite.com',
        env: event.headers.host.split("-")[0] + '-'
    }
    var result = {};
    try {

        var result = await customRoute(event);
        console.log("result", result);

        if (result !== undefined && result !== '') {
            if (result.error) {
                var errMsg = result.message.replace(/"/g, "'");
                return Promise.reject(errMsg)
            } else {
                // Add for payload s3
                const uniqueID = uuidv1()
                const objectLength = JSON.stringify(result).length
                let resultdata = result.data;
                let orgResult = result.data;
                console.log('headers.httpmethod --->', headers.httpmethod);
                if(headers.httpmethod === 'get' && event.config.routeNode === 'analysis'){
                    resultdata = result;
                    orgResult = result;
                }
                if (objectLength > 15000) {
                    //const s3KeyValue = await uploadDatas3(resultdata, uniqueID, s3Params)
                    //resultdata = s3KeyValue
                    const s3KeyValue = { 's3Responsekey': uniqueID }
                    resultdata = s3KeyValue
                            
                var MemcachePlus = require('memcache-plus')
                // var client = new MemcachePlus('stage-wdev-mamcached.udicak.0001.use1.cache.amazonaws.com')

                var client = new MemcachePlus({
                    hosts: ['pro-wdev-mamcached.udicak.cfg.use1.cache.amazonaws.com'],
                    autodiscover: true,
                    maxValueSize: 9999999
                });

                await client.set(uniqueID,orgResult,100);
                console.log('Successfully set the object');

                }
                return Promise.resolve(resultdata)
            }
        } else {
            return Promise.reject(`Not found ${config.name} - ${config.routeNode} node`);
        }

    } catch (error) {
        console.error(config.routeNode, error);
        return Promise.reject(error);
    }

};

var uploadDatas3 = async function (payload, filename, s3Params) {
    const s3Path = s3Params.s3Bucket + '/' + s3Params.env + 'wdev/52137/' + s3Params.appId + '/responseapi'
    filename = filename + '.json'

    return new Promise(function (resolve, reject) {
        fs.writeFile('/tmp/' + filename, JSON.stringify(payload), function (err) {
            if (err) {
                console.log('writeToTmp Failed ' + err)
            } else {
                console.log('writeFile succeeded')

                const fileContent = fs.readFileSync('/tmp/' + filename)
                const params = {
                    Bucket: s3Path,
                    Key: filename,
                    Body: fileContent,
                    ACL: 'public-read'
                }
                // Uploading files to the bucket
                s3.upload(params, function (err, data) {
                    if (err) {
                        reject(err)
                    }
                    var data = { s3Responsekey: s3Path + '/' + filename }
                    resolve(data)
                })
            }
        })
    })
}