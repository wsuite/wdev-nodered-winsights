const qs = require("qs");

module.exports = (event, config, headers) => {
	let query = config.query;
	let table = config.table;
	let table1 = config.table1;
	let table2 = config.table2;
	let join_on = config.join_on;
	var Headerquery = headers.query;
	var resultObj = qs.parse(Headerquery, true);
	let fields = "";
	let where = "";

	if (query) {
		if (resultObj.fields) {
			fields = resultObj.fields.join(",");
		}

		try {
			if (resultObj.filter) {
				resultObj.filter.forEach((element, index) => {
					if (element.length > 0 && element[0].constructor === Array) {
						element.forEach((subElement, subIndex) => {
							if (subIndex > 0) {
								where += " or ";
							}
							where += subElement.join(" ");
						});
					} else {
						if (index > 0) {
							where += " and ";
						}
						where += element.join(" ");
					}
				});
			}
		} catch (error) {
			console.log("error", error);
		}

		let constructQuery = query.split("{columns}");
		query = constructQuery[0] + " " + fields + " " + constructQuery[1];

		if (table) {
			constructQuery = query.split("{table}");
			query = constructQuery[0] + " " + table + " " + constructQuery[1];
		}
		if (table1) {
			constructQuery = query.split("{table1}");
			query = constructQuery[0] + " " + table1 + " " + constructQuery[1];
		}
		if (table2) {
			constructQuery = query.split("{table2}");
			query = constructQuery[0] + " " + table2 + " " + constructQuery[1];
		}
		if (join_on) {
			constructQuery = query.split("{join_on}");
			query = constructQuery[0] + " " + join_on + " " + constructQuery[1];
		}

		constructQuery = query.split("{where}");
		query = constructQuery[0] + "" + where + "" + constructQuery[1];

		if (resultObj.groupBy) {
			constructQuery = query.split(";");
			query = constructQuery[0] + " group by " + resultObj.groupBy + ";";
		}
		if (resultObj.orderBy) {
			constructQuery = query.split(";");
			query = constructQuery[0] + " order by " + resultObj.orderBy + ";";
		}
		if (resultObj.limit && resultObj.limit > 0) {
			constructQuery = query.split(";");
			query = constructQuery[0] + " limit " + resultObj.limit + ";";
		}

		if (resultObj.offset && resultObj.offset > 0) {
			constructQuery = query.split(";");
			query = constructQuery[0] + " offset " + resultObj.offset + ";";
		}
		return query;
	}
	return "";
};
