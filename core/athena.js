const AWS = require("aws-sdk");
const athena = new AWS.Athena({
  region: "us-east-1"
});

QueryExecutionId = '';

/**
 * @description createQueryExecutionId, execute for generating queryExecutionId
 * @param query query to get data from athena
 * @param {Function} callback
 */
function createQueryExecutionId(query, database, callback) {
  /**doing resultConfiguration, but we will not save query result there. */
  const params = {
    QueryExecutionContext: {
      Database: database
    },
    QueryString: query /* required */,
    ResultConfiguration: {
      /* required */
      OutputLocation: `s3://winsights-data/warehouse_v2/24762/full_projects/project_results/` /* required */
    }
  };
  athena.startQueryExecution(params, function(err, data) {
    // console.log("this.QueryExecutionId ==> ", data);
    this.QueryExecutionId = data.QueryExecutionId;
    callback(err ? err.stack : err, data);
  });
}
/**
 * @description checkQueryCreateStatus, check query status till it is not active.
 */
function checkQueryCreateStatus(callback) {
  const params = {
    QueryExecutionId: this.QueryExecutionId /* required */
  };
  athena.getQueryExecution(params, function(err, data) {
    if (err) console.log(err, err.stack);
    // an error occurred
    else {
      if (data && data.QueryExecution && data.QueryExecution.Status && data.QueryExecution.Status.State && data.QueryExecution.Status.State === "RUNNING") {
        console.log("Athena Query status is running");
        callback("RUNNING");
      }else if (data && data.QueryExecution && data.QueryExecution.Status && data.QueryExecution.Status.State && data.QueryExecution.Status.State === "QUEUED") {
        console.log("Athena Query status is Queued");
        callback("QUEUED");
      } else {
        console.log("Athena Query status is Active");
        callback(err ? err.stack : err, data);
      }
    }
  });
}
/**
 * @description getQueryResultByExecutionId, execute for generating result based on queryExecutionId
 * @param {String} queryExecutionId
 * @param {Function} callback
 */
function getQueryResultByExecutionId(queryExecutionId, callback) {
  const params = {
    QueryExecutionId: queryExecutionId
  };
  athena.getQueryResults(params, function(err, data) {
    // console.log(err, data)
    callback(err ? err.stack : err, data);
  });
}

/**
 * @description stopQueryExecutionId, execute for stop queryExecutionId
 * @param {String} queryExecutionId
 * @param {Function} callback
 */
function stopQueryExecutionId(queryExecutionId, callback) {
  const params = {
    QueryExecutionId: queryExecutionId
  };
  athena.stopQueryExecution(params, function(err, data) {
    callback(err ? err.stack : err, data);
  });
}

module.exports = {
  createQueryExecutionId: createQueryExecutionId,
  checkQueryCreateStatus: checkQueryCreateStatus,
  getQueryResultByExecutionId: getQueryResultByExecutionId,
  stopQueryExecutionId: stopQueryExecutionId
};
