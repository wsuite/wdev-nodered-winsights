const qs = require('qs');

module.exports = (config, headers) => {
  let table = config.table;
  var Headerquery = headers.query;
  var resultObj = qs.parse(Headerquery, true);

  let where = "";
  if (resultObj.filter) {
    resultObj.filter.forEach((element, index) => {
      if (index > 0) {
        where += ' and ';
      }
      where += element.join(" ");
    });
  }
  const customQuery = `select (SELECT COUNT(task_id) FROM ${table} WHERE ${where}) as task_total,
  (select count(*) as cc from (SELECT COUNT(client_id) FROM ${table} WHERE ${where} group by client_id) cc) as client_total,
  (select count(*) as pc from (SELECT COUNT(project_id) FROM ${table} WHERE ${where} group by project_id) pc) as project_total ;`;
  
  return customQuery;
};
