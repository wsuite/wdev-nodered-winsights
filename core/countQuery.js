module.exports = dataQuery => {
	let constructQuery = dataQuery;

	if (constructQuery.indexOf("limit") > -1) {
		constructQuery = constructQuery.split("limit")[0];
	}

	if (constructQuery.indexOf("offset") > -1) {
		constructQuery = constructQuery.split("offset")[0];
	}

	if (constructQuery.indexOf(";") > -1) {
		constructQuery = constructQuery.split(";")[0];
	}
	return `SELECT COUNT(*) AS total_records FROM (${constructQuery}) total_records;`;
};
