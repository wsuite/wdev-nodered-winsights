const qs = require("qs");

module.exports = headers => {
	var Headerquery = headers.query;
	var resultObj = qs.parse(Headerquery, true);

	let where = "";
	try {
		if (resultObj.filter) {
			resultObj.filter.forEach((element, index) => {
				if (index > 0) {
					where += " and ";
				}
				where += element.join(" ");
			});
		}
	} catch (error) {
		console.log("error", error);
	}

	let query = `
	SELECT A.idtarea, A.subject, A.horasestimadas, B.horastrabajadas, A.status_name, A.priority_id
	FROM
	(
		SELECT idtarea,
		(CASE
			WHEN horasestimadas IS NULL THEN 0
		 ELSE horasestimadas
		END)horasestimadas,
		assigned_to_login, subject, status_name, priority_id
		FROM project_data
		WHERE ${where}
	) A
	LEFT JOIN
	(
	SELECT idtarea, sum(horastrabajadas) horastrabajadas, login
		FROM project_data
		GROUP BY idtarea, login

	) B
	ON A.idtarea = B.idtarea AND A.assigned_to_login = B.login`;

	if (resultObj.limit && resultObj.limit > 0) {
		constructQuery = query.split(";");
		query = constructQuery[0] + " limit " + resultObj.limit + ";";
	}

	if (resultObj.offset && resultObj.offset > 0) {
		constructQuery = query.split(";");
		query = constructQuery[0] + " offset " + resultObj.offset + ";";
	}

	return query;
};
