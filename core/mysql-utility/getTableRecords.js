const mysqlUtility = {};
const response = {
    error: false,
    msg: "",
    data: null
};

mysqlUtility.getRecords = async (connection, query) => {
    return new Promise((resolve, reject) => {
        try {
            connection.query(query, (err, data) => {
                if (err) {
                    response.error = true;
                    response.msg = err.sqlMessage;
                    reject(response);   
                } else {
                    resolve(response.data = data)
                }
            }); 
        } catch (error) {
            reject(error);
        }
    });
}

module.exports = mysqlUtility;