var mysql = require('mysql');
var mysqlUtility = {};

mysqlUtility.dbConnection = (config) => {
    const connection =  mysql.createConnection({
        host: config.dbhost,
        user: config.dbusername,
        password: config.dbpassword,
        database: config.dbname
    });
    return connection;
};

module.exports = mysqlUtility;
