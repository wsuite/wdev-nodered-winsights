module.exports = {
    ...require('./connection'),
    ...require('./getTableRecords')
}