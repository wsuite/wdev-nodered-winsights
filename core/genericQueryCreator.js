const qs = require("qs");

module.exports = (config, headers, method) => {
    let query = config.query;
    let table = config.table;
    var Headerquery = headers.query;
    var resultObj = qs.parse(Headerquery, true);
    let fields = "";
    let where = "";
    let field_values = "";
    let id = null;
    switch (method) {
        //------------------------------------------- READ Start -----------------------------------------//
        case 'read':
            fields = fieldsJoin(resultObj);
            where = buildWhereCondition(resultObj);
            query = `SELECT ${fields} FROM ${table};`;
            if (where != "") {
                constructQuery = query.split(";");
                query = constructQuery[0] + ` WHERE ${where};`
            }

            if (resultObj.orderBy) {
                constructQuery = query.split(";");
                query = constructQuery[0] + " order by " + resultObj.orderBy + ";";
            }
            if (resultObj.limit && resultObj.limit > 0) {
                constructQuery = query.split(";");
                query = constructQuery[0] + " limit " + resultObj.limit + ";";
            }

            if (resultObj.offset && resultObj.offset > 0) {
                constructQuery = query.split(";");
                query = constructQuery[0] + " offset " + resultObj.offset + ";";
            }
            return query;
            break;
        //------------------------------------------- READ End -------------------------------------------//

        //------------------------------------------- INSERT Start ---------------------------------------//
        case 'write':
            final_column_fields = "";
            field_values = "";
            fields = Object.keys(resultObj.args[0]);
            fields.forEach(ele => {
                field_values += "'" + resultObj.args[0][ele] + "',";
            });
            field_values = field_values.split(",").filter(x => x).join(",");
            query = `INSERT INTO ${table} (${fields}) VALUES (${field_values})`;
            break;
        //------------------------------------------- INSERT End -----------------------------------------//

        //------------------------------------------- UPDATE Start ---------------------------------------//
        case 'update':
            field_values = "";
            where = buildWhereCondition(resultObj);
            fields = Object.keys(resultObj.args[0]);
            let update_query = [];
            id = resultObj.id;
            fields.forEach(ele => {
                field_values += ele = resultObj.args[0][ele] + ",";
            });
            for (let index = 0; index < fields.length; index++) {
                const element = fields[index];
                update_query.push(element + " = '" + resultObj.args[0][element] + "',")
            }
            update_query = update_query.join(",").split(",").filter(x => x).join(",");
            query = `UPDATE ${table} SET ${update_query} WHERE ${where}`;
            break;
        //------------------------------------------- UPDATE End -----------------------------------------//

        //------------------------------------------- DELETE Start ---------------------------------------//
        case 'delete':
            where = buildWhereCondition(resultObj);
            query = `DELETE FROM ${table} WHERE ${where}`;
            break;
        //------------------------------------------- DELETE End -----------------------------------------//
        default:
            query = "";
            break;
    }
    console.log("Final QUery before execution", query);
    return query;
};


function fieldsJoin(obj) {
    let fields = "";
    if (obj.fields) {
        fields = obj.fields.join(",");
    }
    return fields;
}

function buildWhereCondition(resultObj) {
    try {
        let whereString = "";
        if (resultObj.filter) {
            resultObj.filter.forEach((element, index) => {
                if (element.length > 0 && element[0].constructor === Array) {
                    element.forEach((subElement, subIndex) => {
                        if (subIndex > 0) {
                            whereString += " or ";
                        }
                        whereString += subElement.join(" ");
                    });
                } else {
                    if (index > 0) {
                        whereString += " and ";
                    }
                    whereString += element.join(" ");
                }
            });
        }
        return whereString;
    } catch (error) {
        console.log("error", error);
        return "";
    }
}