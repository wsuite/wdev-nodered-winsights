let AWS = require("aws-sdk");
const Athena = require('./athena');
const async = require('async');

module.exports = class execute {
    constructor(accessKey, secretKey, database) {
            new AWS.S3({ accessKeyId: accessKey, secretAccessKey: secretKey });
            this.database = database;
        }
        /**
         * get dynamically table data
         * @param event get api request data
         */
    async getTableData(event) {
        AWS.config.update({ region: "us-east-1" }); // configure region
        return new Promise((resolve, reject) => {
            let params = event.Payload || event.payload;

            console.log('query --->', query);
            console.log('params.query --->', params.query);
            if (params.query === "") {
                reject("Empty Query, please pass query");
            }
            async.waterfall([
                (callback) => {
                    Athena.createQueryExecutionId(params.query, this.database, callback);
                },
                (query, callback) => {
                    async.retry({
                        times: 60,
                        interval: 1000
                    }, Athena.checkQueryCreateStatus.bind(query), (err, result) => {
                        if (!err) {
                            callback(null, query)
                        } else {
                            callback(err)
                        }
                    });
                },
                (query, callback) => {
                    Athena.getQueryResultByExecutionId(query.QueryExecutionId, (err, result) => {
                        callback(err, result, query)
                    })
                },
                (queryResult, query, callback) => {
                    Athena.stopQueryExecutionId(query.QueryExecutionId, (err, result) => {
                        callback(err, queryResult)
                    });
                }
            ], (error, result) => {
                if (error) {
                    reject(error);
                } else {
                    var athenaValules = [];
                    var athenaFields = [];
                    if (result && result.ResultSet && result.ResultSet.Rows) {
                        const clientReportData = result.ResultSet.Rows;
                        if (clientReportData.length > 1) {
                            clientReportData[0].Data.forEach(headerData => {
                                athenaFields.push(headerData.VarCharValue);
                            });
                            clientReportData.forEach((headerData, index) => {
                                if (index != 0) {
                                    var tmp = {};
                                    athenaFields.forEach((fieldData, fieldIndex) => {
                                        tmp[fieldData] = headerData["Data"][fieldIndex]["VarCharValue"];
                                    });
                                    athenaValules.push(tmp);
                                }
                            });
                        }
                    }
                    resolve(athenaValules);
                }
            })
        });
    }
};