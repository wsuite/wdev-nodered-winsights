from index import lambda_handler

event = {
  "Payload": {
    "ds_type":"exacttarget",
    "ds_name": "ds_name_1548139859365",
    "current_step": "init",
    "config": "{\r\n  \"app\": {\r\n      \"reports\": [\r\n        {\r\n          \"name\": \"ListSend\",\r\n          \"properties\": [\r\n            \"Duplicates\",\r\n            \"ExistingUndeliverables\",\r\n            \"ExistingUnsubscribes\",\r\n            \"ForwardedEmails\",\r\n            \"HardBounces\",\r\n            \"InvalidAddresses\",\r\n            \"MissingAddresses\",\r\n            \"NumberDelivered\",\r\n            \"NumberSent\",\r\n            \"OtherBounces\",\r\n            \"PartnerKey\",\r\n            \"PreviewURL\",\r\n            \"SendID\",\r\n            \"SoftBounces\",\r\n            \"UniqueClicks\",\r\n            \"UniqueOpens\",\r\n            \"Unsubscribes\"\r\n          ]\r\n        }\r\n      ]\r\n    },\r\n    \"credentials\": {\r\n      \"clientId\": \"wk7hqtrrhusn1ia47o4nzkp3\",\r\n      \"clientSecret\": \"oip3D1u2TRhiVCGuQkPO5L0x\"\r\n    },\r\n    \"send_payload\":false\r\n  }",
    "run_id": "123333",
    "start_time" : 1265368510000,
    "end_time" : 1551784510000,
    "send_payload": False,
    "type": "data_provider"
  },
  "Config": {
    "b18c802e_ef314": {
      "name": "Process-Task-Assignment"
    }
  },
  "headers": {
    "authorizetoken": "eyJraWQiOiJXZVJpbmpKVForRmxmVzEwTlhsb0dVNFlLXC8xQThaRytFcmRVNGlGU3ZXST0iLCJhbGciOiJSUzI1NiJ9.eyJzdWIiOiJhMzhkYWJkYi03ODM1LTRjMTctYTEzNS02ZWE1ZjZhOTZkM2QiLCJldmVudF9pZCI6ImI0MTE3ZjlhLTY2ODEtNDE0ZC1iODkxLWJiZmI5NWJmYmUxOSIsInRva2VuX3VzZSI6ImFjY2VzcyIsInNjb3BlIjoiYXdzLmNvZ25pdG8uc2lnbmluLnVzZXIuYWRtaW4iLCJhdXRoX3RpbWUiOjE1OTY2MDE3OTIsImlzcyI6Imh0dHBzOlwvXC9jb2duaXRvLWlkcC51cy1lYXN0LTEuYW1hem9uYXdzLmNvbVwvdXMtZWFzdC0xXzJ0TmpwcXNjNSIsImV4cCI6MTU5NjYwNTM5MiwiaWF0IjoxNTk2NjAxNzkyLCJqdGkiOiI3ZWJmMzNmYS0wMGRlLTRkMTItODk5NS1kZTAwNzgyNDU3ZTAiLCJjbGllbnRfaWQiOiIzb2I5bzRrMXJkcnVjcGdzODZjYjR1aHZibiIsInVzZXJuYW1lIjoiY2hpbnRhbi5wYXRlbEBxZGF0YS5pbyJ9.4atZbRvVQkMUDWX5melIhKFfUpa5pjbDgBQlCJ5knX-eiqnPWSZTzo-NNYTkQb9T78Hy2kTQPbVltL-eYUec7XgHy2TgNSFhv_3M8SeSTPv28jvBc8y7bLook21-4_l80mfZ7ImtQo_cmf521VCX9apXEXyOOn_q0rS7jAA-RV5VUibki0z8sN_z18FADUaUE9FdHhaBiPRg3QMJrEnhiCaHgcZh_j2pONBpAvL4yVkrnFf4CHfuhxYOP58P8KCa5QnoOUYFvtkg51Y0DYJCX9MhV0GZGjHYnCC4fnXLjxtpG-FjZaxjvhTtEBdnD_UgUqV1f85fBWU3QtWcdbN2-A",
    "session_id": "session_id=16fabccca98b0634763239fe6ebc64b168650d88",
    "extraparams": "{\"partner\":\"peru\",\"environment\":\"production\",\"idtoken\":\"eyJraWQiOiJcLzdkeklCVW9xelRXZ1wveDlEekxpWkg0blBHYWFqSUhDUHNNeTRIUzdwMmM9IiwiYWxnIjoiUlMyNTYifQ.eyJzdWIiOiJhMzhkYWJkYi03ODM1LTRjMTctYTEzNS02ZWE1ZjZhOTZkM2QiLCJhdWQiOiIzb2I5bzRrMXJkcnVjcGdzODZjYjR1aHZibiIsImV2ZW50X2lkIjoiYjQxMTdmOWEtNjY4MS00MTRkLWI4OTEtYmJmYjk1YmZiZTE5IiwidG9rZW5fdXNlIjoiaWQiLCJhdXRoX3RpbWUiOjE1OTY2MDE3OTIsImlzcyI6Imh0dHBzOlwvXC9jb2duaXRvLWlkcC51cy1lYXN0LTEuYW1hem9uYXdzLmNvbVwvdXMtZWFzdC0xXzJ0TmpwcXNjNSIsIm5hbWUiOiJDaGludGFuIiwiY29nbml0bzp1c2VybmFtZSI6ImNoaW50YW4ucGF0ZWxAcWRhdGEuaW8iLCJleHAiOjE1OTY2MDUzOTIsIm1pZGRsZV9uYW1lIjoiUGF0ZWwiLCJpYXQiOjE1OTY2MDE3OTIsImVtYWlsIjoiY2hpbnRhbi5wYXRlbEBxZGF0YS5pbyJ9.dpdnHjt2JtDDP7YtSfvyaXZnyLXTKNVMI15oU9euCDJvnRWgkyf3PVNSlEqYdPVLOKmS7bBmrIbiMRIfvqQzSj3ns3j1CcQpOWs2aZwRHE8_41f5ki3RQxmUKaBAJOw7UwALx59ob9LZCif9N3QecSln_wOjLWfhnhcDoz2Tl7XtV9v7G0PMpM329wTSO4wTCanYH9oZpgRPaIBZWe-aJ4dEz_1U5He2NX73pQLHjFjPQYPUqWnvfp497ygRsR47PqK9vN4YWBxQjXsgArNWXJsPnJm4WMdaGc05EuH2HeyJL58O9poUkRs_9IdjLaXHoTTb4UA0R5s5vJPWTNEu7g\"}",
    "host": "services-api.wsuite.com",
    "httpmethod": "post",
    "params": "peru-plan/assigntask",
    "query": {}
  },
  "serviceConfig": {
    "details": {
      "erp_host": "wsuiteerp.wsuite.com",
      "db": "wsuite_plan",
      "host": "production-58701-mysqldb.wsuite.com",
      "username": "plan_user",
      "password": "Lnpvh36rcUiEobJ7RlAMxGYg0"
    }
  }
}

context = {}


result = lambda_handler(event, context)
print(result)