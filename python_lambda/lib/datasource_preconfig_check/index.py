#!/usr/bin/env python
# -*- coding: utf-8 -*-
import json, sys 

def lambda_handler(event, context):
    body = event
    if not body or not body['config'] :
        return {
            'statusCode': 400,
            'body': json.dumps('Missing datasource config')
        }
    
    if not body or not body['row'] :
        return {
            'statusCode': 400,
            'body': json.dumps('Missing sample row')
        }

    if not body or not body['function_code'] :
        return {
            'statusCode': 400,
            'body': json.dumps('Missing function code')
        }

    if not body or not body['handler_name'] :
        return {
            'statusCode': 400,
            'body': json.dumps('Missing handler name')
        }
    
    ds_config = body['config']
    row = json.dumps(body['row'])
    functionCode = body['function_code']
    functionName = body['handler_name']
    exec(functionCode)
    try:
        functionOutput = locals()[functionName](row, ds_config)
        try:
            json.loads(functionOutput)
            message = 'function_completed'
            functionOutput = json.loads(functionOutput)
        except Exception as e:
            message = 'function_not_completed'
        
    except Exception as e:
        return {
            'statusCode': 400,
            'body': {'data':e.message, 'message': 'function_not_completed'}
        }
    

    return {
        'statusCode': 200,
        'body': {'data':functionOutput, 'message': message}
    }