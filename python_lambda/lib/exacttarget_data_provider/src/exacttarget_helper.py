import boto3
import sys
import os
import json
import FuelSDK
from FuelSDK import rest

lambdaClient = boto3.client('lambda', region_name='us-east-1')

"""
Global ET Client to follow recomendation of the github page
'Do not instantiate a new ET_Client object for each request made.'
"""
ETClient = None

"""
  Generic class to retrive information for a given endpoint
"""
class ET_RetriveGeneric(rest.ET_CUDSupport):
    subscribers = None
    attributes = None
    def __init__(self, endpoint_name):
        super(ET_RetriveGeneric, self).__init__()
        self.obj_type = endpoint_name

def ETResponse2PythonObject(response, properties):
    ret = []
    for item in response:
        val = {}
        for prop in properties:
            val[prop] = item[prop]
        ret.append(val)
    return ret

def GenericRetrive(endpoint, properties, search_filter=None):
    global ETClient
    if ETClient is None:
        try:
            ETClient = FuelSDK.ET_Client()
        except Exception as e:
            return False, str(e)
    list = ET_RetriveGeneric(endpoint)
    list.auth_stub = ETClient
    list.props = properties
    if search_filter is not None:
        list.search_filter = search_filter
    response = list.get()
    if not response.status:
        return False, response.message
    else:
        return True, response.results
