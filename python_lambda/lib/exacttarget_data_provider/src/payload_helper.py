import boto3
import sys
import json

Lambda = boto3.client('lambda', region_name='us-east-1')
S3 = boto3.resource('s3', region_name='us-east-1')
DynamoDB = boto3.resource('dynamodb', region_name='us-east-1')

OBJECT_LIMIT = 5000000
PROCESS_LAMBDA = {"python": 'python-preprocess-lambda', "nodejs": 'python-preprocess-lambda'}
BUCKET_NAME = 'winsights-data'


def sendPayload(payload):
    configResult = None
    configError = None
    configResult = getConfigData(payload["ds_name"])
    runtimeEnv = getFunctionDetail(configResult["Item"])
    processData(payload, configError, configResult, runtimeEnv)

def getConfigData(datasource):
    table = DynamoDB.Table('winsights-client-datasources')
    response = table.get_item(
        Key={
            'name': datasource
        }
    )
    #response = table.query(
    #    KeyConditionExpression=Key('name').eq(datasource)
    #)  
    return response

def getFunctionDetail(itemResponse): 
    if (len(itemResponse['pre_config']) == 0):
        return None
    else:
        functionDetails = []
        for step in itemResponse['pre_config']:
            if(step['order'] == 1):
                functionDetails.push(step['order'])
        if(len(functionDetails) == 0):
            return None
        else:
            return functionDetails[0]['runtime']

def processData(payload, error, data, runtimeEnv):
    objectSize = sys.getsizeof(payload)
    runtime = runtimeEnv if runtimeEnv else 'python'
    if(objectSize > OBJECT_LIMIT):
        if(error):
            return None
        else:
            key = 'datasource_inputs/' + data['Item']['wclient_id'] + '/' + data['Item']['name'] + '/input_' + '.json';
            bucket = s3.Bucket(BUCKET_NAME)
            bucket.put_object(
                Key=key,
                Body=json.dumps(payload),
            )
            response = Lambda.invoke(
                FunctionName=PROCESS_LAMBDA[runtime],
                InvocationType='RequestResponse',
                Payload=json.dumps(payload)
            )
            # S3 Put Object
            # Invoke Lamnda   
    else:
        payload['type'] = 'event'
        response = Lambda.invoke(
                FunctionName=PROCESS_LAMBDA[runtime],
                InvocationType='RequestResponse',
                Payload=json.dumps(payload)
            )
    
    return response    
    

        
    
