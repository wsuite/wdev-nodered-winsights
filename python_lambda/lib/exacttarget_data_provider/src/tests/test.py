import sys
import json
import os
from index import handler

"""
  For testing proposes, use python index.py /path/to/event/test.json to test the
  python data source with a custon event, default: ../config/event-test.json
"""
if __name__ == '__main__':
  class AWSContextStub(object):
    def done(self, err, success):
      print("Finished Main function => ", str(err), str(success))
  
  current_dir = os.path.dirname(__file__) + '/'
  filename = sys.argv[1] if len(sys.argv) >= 2 else current_dir + '../../assets/event-test.json'
  with open(filename) as f:
    event = json.load(f)

  handler(event, AWSContextStub())
