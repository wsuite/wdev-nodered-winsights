from ..src.exacttarget_helper import ETResponse2PythonObject, GenericRetrive

def transform_func(event, metric_name, data):
    if metric_name == 'ListSend':
        print("Applying post-processing to ListSend")
        return list_send_postprocessing(data)
    else:
        return data


def list_send_postprocessing(rsp):
    send_ids = {}
    def post_process(cursor, index):
        cursor['index'] = index + 1
        if 'PreviewURL' in cursor:
            cursor['EmailPreview'] = cursor['PreviewURL']

        if 'SendID' in cursor:
            cursor['JobID'] = cursor['SendID']
            if cursor['SendID'] in send_ids:
                cursor['Name'] = send_ids[cursor['SendID']]['EmailName']
                cursor['Subject'] = send_ids[cursor['SendID']]['Subject']

        if 'CreatedDate' in cursor:
            cursor['SendStartTime'] = cursor['CreatedDate']

        if 'NumberSent' in cursor:
            cursor['EmailsSent'] = cursor['NumberSent']

        if 'NumberDelivered' in cursor:
            cursor['EmailsDelivered'] = cursor['NumberDelivered']

        if 'EmailsSent' in cursor and 'EmailsDelivered' in cursor:
            cursor['Undeliverable'] = cursor['EmailsSent'] - \
                cursor['EmailsDelivered']

        if 'UniqueClicks' in cursor:
            cursor['UniqueClickThroughs'] = cursor['UniqueClicks']

        if 'UniqueOpens' in cursor:
            cursor['UniqueEmailsOpened'] = cursor['UniqueOpens']

        if 'HardBounces' in cursor:
            cursor['Unsubscribes'] = cursor['HardBounces']

        if 'EmailsDelivered' in cursor and 'UniqueEmailsOpened' in cursor:
            if cursor['EmailsDelivered'] == 0:
                cursor['OpenRate'] = 0
            else:
                cursor['OpenRate'] = \
                  (cursor['UniqueEmailsOpened'] / cursor['EmailsDelivered']) * 100

        if 'EmailsSent' in cursor and 'EmailsDelivered' in cursor:
            if cursor['EmailsSent'] == 0:
                cursor['DeliverabilityRate'] = 0
            else:
                cursor['DeliverabilityRate'] = \
                    (cursor['EmailsDelivered'] / cursor['EmailsSent']) * 100

        if 'EmailsSent' in cursor and 'Undeliverable' in cursor:
            if cursor['EmailsSent'] == 0:
                cursor['BounceRate'] = 0
            else:
                cursor['BounceRate'] = \
                    (cursor['Undeliverable'] / cursor['EmailsSent']) * 100

            if 'EmailsDelivered' in cursor and 'Unsubscribes' in cursor:
                if cursor['EmailsDelivered'] == 0:
                    cursor['UnsubscribeRate'] = 0
                else:
                    cursor['UnsubscribeRate'] = \
                        (cursor['Unsubscribes'] / cursor['EmailsDelivered']) * 100

        if 'EmailsDelivered' in cursor and 'UniqueClickThroughs' in cursor:
            if cursor['EmailsDelivered'] == 0:
                cursor['UniqueClickThroughRate'] = 0
            else:
                cursor['UniqueClickThroughRate'] = \
                    (cursor['UniqueClickThroughs'] / cursor['EmailsDelivered']) * 100

        return cursor

    def get_sends(rsp):
        send_list = []
        ret_obj = {}
        for cursor in rsp:
            if 'SendID' in cursor:
                send_list.append(cursor['SendID'])
        if len(send_list) > 0:
            send_list = list(set(send_list))
            send_properties = ['ID', 'EmailName', 'Subject']
            ret, rsp = GenericRetrive('Send', send_properties, {
                'Property': 'ID',
                'SimpleOperator': 'IN',
                'Value': send_list
            })
            if not ret or len(rsp) != len(send_list):
                print("Error retriving information for Send with ID" + str(cursor['SendID']))
            else:
                rsp = ETResponse2PythonObject(rsp, send_properties)
                for cursor in rsp:
                    ret_obj[cursor['ID']] = cursor
        return ret_obj

    send_ids = get_sends(rsp)
    ret = [post_process(cursor, rsp.index(cursor)) for cursor in rsp]
    return ret
