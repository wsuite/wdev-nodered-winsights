# Install dependencies to the 'package' folder
# Warning use Python 3 and PiP for python 3

# 1- Change dir to the exacttarget folder (exacttarget-ds-lambda/)
cd exacttarget-ds-lambda/

# 2- Create a folder to install the dependencies, it should be called 'package'
mkdir package

# 3- Install the dependencies using pip
pip install -r requirements.txt --target package

# 4.1- Test the index.py using a test event for SendList
python3 -m src.tests.test assets/event-test.json
# The result in file data_output_ListSend.json

# 4.2- Test the index.py using a test event for SendSummary
python3 -m src.tests.test assets/event-test2.json
# The result in file data_output_SendSummary.json

# 4.3- Test the index.py using a test event for Send, this example implements 
# filtering
python3 -m src.tests.test assets/event-test3.json
# The result in file data_output_Send.json
