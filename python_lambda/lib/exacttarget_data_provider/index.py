import boto3
import os, sys
import json
sys.path.append(os.path.join(os.path.dirname(__file__), "src"))

from .src.exacttarget_helper import ETResponse2PythonObject, GenericRetrive
from .transform.transform import transform_func
from .src.payload_helper import sendPayload

lambdaClient = boto3.client('lambda')

def metrics_map(event, eventConfig, context):
    configured_reports = eventConfig['app']['reports']
    for metric in configured_reports:
        custom_handlers = []
        if metric['name'] in custom_handlers:
            print("Implement a special handler for this metric")
        else:
            search_filter = \
                metric['searchFilter'] if 'searchFilter' in metric else None
            ret, rsp = GenericRetrive(metric['name'], metric['properties'], search_filter)
            if not ret:
                print("Error retriving information for " + metric['name'])
                if eventConfig['send_payload']:
                    userData = eventConfig['user_details']
                    emailPayload = {
                        "firstName" : userData['name'],
                        "lastName"  : userData['last_name'],
                        "email"     : userData['email'],
                        "error"     : rsp,
                        "analysisID" : eventConfig['analysis_id']   
                    }
                    res = lambdaClient.invoke(
                            FunctionName='wsuite-email-notification',
                            InvocationType='RequestResponse',
                            Payload=json.dumps(emailPayload)
                        )
                return {
                    "errorMessage": rsp,
                    "code": 400
                }

            rsp = ETResponse2PythonObject(rsp, metric['properties'])
            print(metric['name'], str(len(rsp)))

            rsp = transform_func(eventConfig, metric['name'], rsp)
            
            if eventConfig['send_payload']:
                event['current_step'] = 'init'
                if eventConfig['step'] == 'jobs':
                    event['data'] = json.dumps([rsp[0]])
                else:        
                    event['data'] = json.dumps(rsp)
                    
                res = sendPayload(event)
                return {
                        "message": "Success",
                        "code": 200
                    }
            else:
                event['data'] = json.dumps(rsp[0])
                return {
                    "message":"Found data",
                    "code":200,
                    "body":{
                        "data":event['data']
                    }
                }  
"""
  Entry point
"""
def handler(event, context):
    # Add environment variables from the event
    eventConfig = json.loads(event['config'])
    os.environ["FUELSDK_CLIENT_ID"] = eventConfig['credentials']['clientId']
    os.environ["FUELSDK_CLIENT_SECRET"] = eventConfig['credentials']['clientSecret']
    res = metrics_map(event, eventConfig, context)
    return res 
