#!/usr/bin/env python

import os, sys, json
sys.path.append(os.path.join(os.path.dirname(__file__), "lib"))

from lib.datasource_preconfig_check.index import lambda_handler as preconfig_lambda
from lib.exacttarget_data_provider.index import handler as dataprovider_lambda

def lambda_handler(event, context):
    event = event['Payload']
    if(event['type'] == 'config_checker'):
        return preconfig_lambda(event, context)
    elif(event['type'] == 'data_provider'):
        return dataprovider_lambda(event, context)