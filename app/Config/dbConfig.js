var myModel = require('@wsuite-team/ws-mysql-easy-model');

// var connection = myModel.createConnection({
//     host: 'stage-52137-mysqldb.wsuite.com',
//     user: 'wsuite_db_user',
//     password: 'IfgJsq1TRtASoN5jakGBFKiWy',
//     database: 'wsuite_insights'
// });

var connection = myModel.createConnection({
    host: process.env.host,
    user: process.env.username,
    password: process.env.password,
    database: process.env.db
});

// var connection = myModel.createConnection({
//     host:    'wsuite-serverless.cluster-c3uahxgwxkrq.us-east-1.rds.amazonaws.com', 
//     user:     'winsightsdbuser', 
//     password: '5wUI6sjQ*rzu',
//     database: 'winsightdb'
// });


module.exports = myModel