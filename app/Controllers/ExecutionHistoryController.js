const AWS = require('aws-sdk');
const dynamodb = new AWS.DynamoDB();

const DYNAMODB_DS_RUN_HISTORY_TABLE = `${process.env.ENV}-datasource-run-history`;
const DEFAULT_LIMIT = 50;

module.exports = class ExecutionHistoryController {

    constructor() { }

    static async getdata(event) {
        if(!event.queryParam) {
            return Promise.resolve({ error: true, data: null, message: 'Invalid request : missing required parameters' })
        }
        let eventData = event.queryParam
        const params = {
            TableName: DYNAMODB_DS_RUN_HISTORY_TABLE,
            IndexName: "name-start_time-index",
            KeyConditionExpression: "#name = :d",
            ExpressionAttributeValues: {
                ":d": {
                    S: eventData.ds_name
                }
            },
            ExpressionAttributeNames: {
                "#name": "name"
            },
            ReturnConsumedCapacity: "TOTAL",
            Limit: eventData.Limit || DEFAULT_LIMIT,
            ScanIndexForward: false,
        };
    
        try {
            if (eventData.LastEvaluatedKey) {
                console.log(eventData.LastEvaluatedKey);
                params.ExclusiveStartKey = JSON.parse(eventData.LastEvaluatedKey);
            }
        
            let response = {}
            let queryRes = await dynamodb.query(params).promise()
            response.data = queryRes.Items.map((item) => AWS.DynamoDB.Converter.unmarshall(item))
            if (queryRes.LastEvaluatedKey)
                response.LastEvaluatedKey = JSON.stringify(JSON.stringify(queryRes.LastEvaluatedKey));
            return Promise.resolve({ error: false, data: response })
        } catch (error) {
            return Promise.resolve({ error: false, data: error, message: 'Error while getting execution history' })
        }
        
    }
}