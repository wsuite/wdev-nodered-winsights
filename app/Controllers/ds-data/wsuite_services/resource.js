var mysql = require('mysql');

var datasource = function (event, cb) {
    const configuration = event.config;
    var env_host = process.env.WSUITE_HOST	
    var env_username = process.env.WSUITE_USERNAME	
    var env_password = process.env.WSUITE_PASSWORD		
    var env_database = process.env.WPLAN_DATABASE
    if(event.service == 'wtrack'){
        env_database = process.env.WTRACK_DATABASE			
    }

    const client = mysql.createConnection({
        host: env_host,
        user: env_username,
        password: env_password
    });

    client.connect(function (err) {        
        if (err) {
            return cb(err);
        }
        else {
            client.query('SELECT  * FROM  `'+env_database+'`.'+configuration.dbtableName[0] + ' LIMIT 1', (error, resp) => {
                if (error) {
                    return cb(error);
                } else {
                    // return resp.rows.length > 0 ? cb(null, resp.rows) : cb(null, {});
                    return cb(null, resp);
                }
            });
        }
    });
}
exports.fetchData = datasource;