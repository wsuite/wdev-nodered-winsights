// modules
const rp = require('request-promise');
const CryptoJS = require('crypto-js');
const path = require('path');
var AWS = require('aws-sdk');
var moment = require('moment');

// JSON-like object containing the event parameters passed in to the lambda 
// function
let config = {};
let eventData = {};

// Object containing lambda specific context properties and methods
let callback = null;

let wsse = null;

let urlBase = null;

const paramsBase = {
    method: 'GET',
    json: true,
};

const WSSE = function (username, secret) {
    this.username = username;
    this.secret = secret;
};

WSSE.prototype.getHeader = function () {
    let auth = [this.username, this.secret].join(':');
    let buffAuth = new Buffer(auth);
    let base64Auth = buffAuth.toString('base64');
    return { 'Authorization': 'Basic ' + base64Auth };
};


/* For socialbaker there are 2 endpoints of metrics associated to each social
   network:
   Twitter -> metrics and Twitter tweets metrics
   Youtube -> metrics and Youtube video metrics
   Facebook -> metrics and Facebook post metrics
   Instagram -> metrics and Instagram post metrics
   
   Printest is the exception with just the metrics endpoint
*/

getProfiles = () => {
    const params = Object.assign(paramsBase, {
        uri: `${urlBase}profiles`,
        headers: wsse.getHeader(),
    });
    return rp(params);
};

getMetrics = (dateStart, dateEnd, profileId, metrics) => {
    const params = Object.assign(paramsBase, {
        uri: `${urlBase}metrics`,
        headers: wsse.getHeader(),
        body: {
            date_start: dateStart,
            date_end: dateEnd,
            profiles: [profileId],
            metrics: metrics
        },
        method: 'POST',
    });
    return rp(params);
};

getProfileMetrics = (dateStart, dateEnd, profileId, fields, profileMetricName, next) => {
    const params = Object.assign(paramsBase, {
        uri: `${urlBase}${profileMetricName}`,
        headers: wsse.getHeader(),
        body: {
            date_start: dateStart,
            date_end: dateEnd,
            profiles: [profileId],
            fields: fields,
            limit: config.app.profileMetricsPagination // 100 approx 63KB
        },
        method: 'POST',
    });
    if (next)
        params.body.after = next;

    return rp(params);
};

getProfileMetricsPagination = (resolve, reject, results, next, dateStart, dateEnd, profileId, fields, profileMetricName) => {
    let remaining = 0;
    getProfileMetrics(dateStart, dateEnd, profileId, fields,
        profileMetricName, next).then(rsp => {
            if (!rsp.success)
                return reject(rsp.errors);
            if (rsp.data && rsp.data.remaining)
                remaining = rsp.data.remaining;

            results.push(rsp);
            if (remaining <= 0)
                return resolve(results);

            return getProfileMetricsPagination(resolve, reject, results,
                rsp.data.next, dateStart, dateEnd, profileId, fields,
                profileMetricName);
        }).catch(error => {
            return reject(error);
        });
};


getSocialMetrics = () => {
    let promises = [];
    if (config.app.type == "metrics") {
        promises.push(getMetrics(moment(Number(eventData.end_time)).subtract(1, "days").format("YYYY-MM-DD"), moment(Number(eventData.end_time)).format("YYYY-MM-DD"),
            config.app.profileIds[0].id, config.app.metrics));
    }

    if (config.app.type == "profile_metrics") {
        let socialName = path.basename(urlBase);
        if (socialName !== 'pinterest') {
            let profileMetricName;
            if (socialName == 'twitter')
                profileMetricName = 'profile/tweets';
            else if (socialName == 'facebook')
                profileMetricName = 'page/posts';
            else if (socialName == 'instagram')
                profileMetricName = 'profile/posts';
            else if (socialName == 'instagram')
                profileMetricName = 'profile/posts';
            else if (socialName == 'youtube')
                profileMetricName = 'profile/videos';

            promises.push(new Promise((resolve, reject) => {
                getProfileMetricsPagination(resolve, reject, [], null,
                    moment(Number(eventData.end_time)).subtract(1, "days").format("YYYY-MM-DD"), moment(Number(eventData.end_time)).format("YYYY-MM-DD"), config.app.profileIds[0].id,
                    config.app.profileFields, profileMetricName);
            }));
        }
    }

    Promise.all(promises).then(rsps => {
        if (rsps.length === 0)
            return callback('No data available', null);

        // If we need metrics
        if(config.app.type == 'metrics') {
            console.log(JSON.stringify(rsps[0]))
            callback(null, rsps[0])
        }
        
        // If we want profile_metrics
        if (config.app.type == 'profile_metrics') {
            // Return only sample record
            callback(null, rsps[0][0])
        }
    }).catch(errors => {
        return callback(errors, null);
    });
};

exports.fetchData = function (event, cb) {
    eventData = event
    config = event.config;
    callback = cb;
    wsse = new WSSE(config.credentials.client, config.credentials.password);
    urlBase = config.credentials.url;
    getSocialMetrics();
};