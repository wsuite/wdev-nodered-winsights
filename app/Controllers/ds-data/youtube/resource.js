var {google} = require('googleapis');
var prettyjson = require('prettyjson');
var moment = require('moment');
var callback = null;
//var analytics = google.analyticsreporting('v4'); 
const OAuth2 = google.auth.OAuth2;

let config = {};
let parameters = {};
let eventData = {};

var pretty_json_options = {
    noColor: false
};
   
getReport = (auth, params) => {
    return new Promise((resolve, reject) => {
        const youtubeAnalytics = google.youtubeAnalytics('v2');
    
        youtubeAnalytics.reports
          .query({
            ids: eventData.channells.toString(), // "channel==MINE",
            startDate: moment.unix(eventData.start_date).format("YYYY-MM-DD"),
            endDate: moment.unix(eventData.end_date).format("YYYY-MM-DD"),
            metrics: eventData.metrics.toString(),
            dimensions: eventData.dimensions.toString(), // "day",
            sort: 'day',
            auth,
          }).then((results) => {
                resolve(results);
          });
      
  });
}

exports.fetchData = function (event, cb) {
    callback = cb;
    eventData = event
    config = event.config;
    parameters = event.params;
    console.log(config);    
    const oauth2Client = new OAuth2(config.clientId, config.clientSecret);
    oauth2Client.credentials = config.token;
    getReport(oauth2Client, parameters).then((result) => {
        callback(null,JSON.stringify(result.data));
    }).catch((err) => {
        callback(err,null);
    });
};


