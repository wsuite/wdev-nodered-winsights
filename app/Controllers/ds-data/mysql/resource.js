var mysql = require('mysql');

var datasource = function (event, cb) {
    const configuration = event.config;
    //console.log('Hello : Event Config:  ', configuration);
    //console.log('Config:  ', configuration.username);

		const client = mysql.createConnection({
			host: configuration.host,
			user: configuration.username,
            password: configuration.password,                        
		  });        
    client.connect(function (err) {        
        if (err) {
            return cb(err);
        }
        else {
            //console.log('QUERY','select  * from  `'+configuration.database+'`.'+configuration.dbtableName[0] + ' LIMIT 1')
            client.query('SELECT  * FROM  `'+configuration.database+'`.'+configuration.dbtableName[0] + ' LIMIT 1', (error, resp) => {
                //console.log('query resp', resp)
                //console.log('query error', error)
                if (error) {
                    return cb(error);
                } else {
                    // return resp.rows.length > 0 ? cb(null, resp.rows) : cb(null, {});
                    return cb(null, resp);
                }
            });
        }
    });
}
exports.fetchData = datasource;