var pgp = require('pg');

var datasource = function (event, cb) {
    const configuration = event.config;
    console.log('Hello : Event Config:  ', configuration);
    console.log('Config:  ', configuration.username);

    const client = new pgp.Client(
        {
            user: configuration.username,
            host: configuration.host,
            database: configuration.database,
            password: configuration.password,
        });
    client.connect(function (err) {
        if (err) {
            return cb(err);
        }
        else {
            client.query('select  * from  public."' + configuration.dbtableName[0] + '" LIMIT 1', (error, resp) => {
                if (error) {
                    return cb(error);
                } else {
                    // return resp.rows.length > 0 ? cb(null, resp.rows) : cb(null, {});
                    return cb(null, resp.rows);
                }
            });
        }
    });
}
exports.fetchData = datasource;