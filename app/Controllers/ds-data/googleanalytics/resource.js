var {google} = require('googleapis');
var moment = require('moment');

var callback = null;
const OAuth2 = google.auth.OAuth2;

let parameters = {};
let eventData = {};
let credentials = {};
   
getReport = (auth, params) => {
    return new Promise((resolve, reject) => {
      const googleAnalyticsReporting = google.analyticsreporting({version: 'v4', auth: auth});

      googleAnalyticsReporting.reports.batchGet({
        requestBody: params})
        .then((result) =>
          resolve(result))
        .catch((error) => {
            console.log('Error while generating Report', error);
            reject(error);
        });
    });
  };
exports.fetchData = function (event, cb) {
    callback = cb;
    eventData = event
    credentials = event.credentials;
    parameters = {
        "reportRequests": [
            {
                "viewId": `${eventData.viewId}`,
                "dateRanges": [
                    {
                        "startDate": `${moment.unix(Number(eventData.start_time)).format("YYYY-MM-DD")}`,
                        "endDate": `${moment.unix(Number(eventData.end_time)).format("YYYY-MM-DD")}`
                    }
                ],
                "metrics": eventData.metrics,
                "dimensions": eventData.dimensions,
                "pageSize": eventData.pageSize
            }
        ]
    }
    const oauth2Client = new OAuth2(credentials.clientId, credentials.clientSecret);
    oauth2Client.credentials = credentials.token;
    getReport(oauth2Client, parameters).then((result) => {
        callback(null,JSON.stringify(result.data));
    }).catch((err) => {
        callback(err,null);
    });
};


