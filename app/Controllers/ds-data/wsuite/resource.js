var mysql = require('mysql');

var datasource = function (event, cb) {
    const configuration = event.config;

    var env_host = process.env.WPROJECT_HOST	
    var env_username = process.env.WPROJECT_USERNAME	
    var env_password = process.env.WPROJECT_PASSWORD
    if(configuration.service == 'wpeople'){
        env_host = process.env.WPEOPLE_HOST	
        env_username = process.env.WPEOPLE_USERNAME	
        env_password = process.env.WPEOPLE_PASSWORD			
    }

    const client = mysql.createConnection({
        host: env_host,
        user: env_username,
        password: env_password
    });

    client.connect(function (err) {        
        if (err) {
            return cb(err);
        }
        else {
            client.query('SELECT  * FROM  `'+configuration.database+'`.'+configuration.dbtableName[0] + ' LIMIT 1', (error, resp) => {
                if (error) {
                    return cb(error);
                } else {
                    // return resp.rows.length > 0 ? cb(null, resp.rows) : cb(null, {});
                    return cb(null, resp);
                }
            });
        }
    });
}
exports.fetchData = datasource;