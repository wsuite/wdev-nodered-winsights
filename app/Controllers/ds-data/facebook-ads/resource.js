// const rp = require('request-promise');
// const CryptoJS = require('crypto-js');
// const fs = require('fs');
// var AWS = require('aws-sdk');
// var lambda = new AWS.Lambda({ apiVersion: '2015-03-31' });
// var moment = require('moment');
// var async = require("async");
// var Facebook = require('fb').Facebook,
//         FB = new Facebook();
// const adsSdk = require('facebook-nodejs-business-sdk');

// JSON-like object containing the event parameters passed in to the lambda 
// function
let config = {};

// Object containing lambda specific context properties and methods
let callback = null;


var access_token, eventData;
const https = require('https');


function getSampleData(config, cb) {
    let campaign_id;
    if (config.fbInsightsType)
        campaign_id = config.fbInsightsType[0] === "Ad Account ID" ? "act_" + config.campaign_id : config.campaign_id;
    console.log("Campaign ID after:", campaign_id);
    let fromDate = config.fromDate;
    let toDate = config.toDate;

    let startTime = `${fromDate.year}-${fromDate.month}-${fromDate.day}`;
    let endTime = `${toDate.year}-${toDate.month}-${toDate.day}`;

    // let startTime = `2018-05-04`;
    // let endTime = `2019-07-04`;

    
    let url = `https://graph.facebook.com/v3.3/${campaign_id}/insights?access_token=${config.access_token}` + "&time_range={'since':'" + startTime + "','until':'" + endTime + "'}";
    Object.keys(config.fbParamSelection).forEach((key) => {
        url += `&${key}=` + config.fbParamSelection[key].join(',');
    });
    console.log(url);
    https.get(url, (resp) => {
        let data = '';

        // A chunk of data has been recieved.
        resp.on('data', (chunk) => {
            data += chunk;
        });

        // The whole response has been received. Print out the result.
        resp.on('end', () => {
            data = JSON.parse(data);
            console.log('data', data);
            console.log('Object.keys(data)', Object.keys(data));
            if (!data.data) {
                cb({
                    'success': false,
                    'message': 'failed to get sample data!',
                    'data': data
                }, null);
            }
            else {
                cb(null, {
                    'success': true,
                    'message': 'sample data fetched',
                    'data': data
                });
            }
        });
    });
}

exports.fetchData = function (event, cb) {
    eventData = event
    config = event.config;
    callback = cb;
    config.campaign_id
    
    getSampleData(config, cb);
};


