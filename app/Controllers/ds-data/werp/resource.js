var pgp = require('pg');

var datasource = function (event, cb) {
    const configuration = event.config;
    console.log('typeof configuration', configuration)


    const env_host = process.env.ERP_HOST
    const env_username = process.env.ERP_USERNAME
    const env_password = process.env.ERP_PASSWORD

    const client = new pgp.Client(
        {
            user: env_username,
            host: env_host,
            database: `werp_${configuration.database[0]}`,
            password: env_password,
        });
    client.connect(function (err) {
        if (err) {
            return cb(err);
        }
        else {
            console.log('1111');
            client.query('select  * from  public."' + configuration.dbtableName[0] + '" LIMIT 1', (error, resp) => {
                console.log('resp', resp)
                console.log('error', error)

                if (error) {
                    return cb(error);
                } else {
                    // return resp.rows.length > 0 ? cb(null, resp.rows) : cb(null, {});
                    return cb(null, resp.rows);
                }
            });
        }
    });
}
exports.fetchData = datasource;