// modules
const rp = require('request-promise');
const path = require('path');
var moment = require('moment');
var kinesis;

// JSON-like object containing the event parameters passed in to the lambda 
// function
let config = {};

let eventData = {};

// Object containing lambda specific context properties and methods
let context = null;

let wsse = null;

let urlBase = null;

const paramsBase = {
    method: 'GET',
    json: true,
};

const WSSE = function (appKey, appSecret) {
    this.appKey = appKey;
    this.appSecret = appSecret;
};

WSSE.prototype.getHeader = function () {
    let auth = [this.appKey, this.appSecret].join(':');
    let buffAuth = new Buffer(auth);  
    let base64Auth = buffAuth.toString('base64');
    return {'Authorization': 'Basic ' + base64Auth};
};
   
getURI = (url) => {
  return `${urlBase}${url}`;
};

getReportBase = (uri, body, queryString) => {
    let params = Object.assign(paramsBase, {
        uri: uri,
        headers: wsse.getHeader(),
    });
    
    if (body)
      params.body = body;

    if (queryString)
      params.qs = queryString;

    return rp(params);
};

// paginationHelper = (resolve, reject, results, next) => {
//     if (!next)
//       return resolve(results);

//     getReportBase(next, null, null).then(rsp => {
//       results.push(rsp);
//       return paginationHelper(resolve, reject, results, rsp.next_page);
//     }).catch(err => {
//       return reject(err);
//     });
// };

/*
* Get an app's opted-in and installed device counts by device type.
* Optional: date
*/
getDevicesReport = (body, queryString) => {
    return getReportBase(getURI('/api/reports/devices'), null, queryString);
};

/*
* Get a summary of custom event counts and values, by custom event, within the 
* specified time period.
* Mandatory: start, end, precision
* Optional: page, page_size
*/
getEventsReport = (body, queryString) => {
    return new Promise((resolve, reject) => {
      getReportBase(getURI('/api/reports/events'), null, queryString)
      .then(rsp => {
        let results = [rsp];
        return resolve(results);
        //return paginationHelper(resolve, reject, results, rsp.next_page);
      }).catch(err => {
        return reject(err);
      });
    });
};

/*
* Returns statistics and metadata about an experiment (A/B Test).
* Mandatory: push_id
*/
getExperimentsOverview = (body, queryString) => {
    let push_id = queryString.push_id;
    return getReportBase(getURI(`/api/reports/experiment/overview/${push_id}`),null, null);
};

/*
* Get a summary of custom event counts and values, by custom event, within the 
* specified time period.
* Mandatory: start, end, precision
*/
getOpensReport = (body, queryString) => {
    return new Promise((resolve, reject) => {
      getReportBase(getURI('/api/reports/opens'), null, queryString)
      .then(rsp => {
        let results = [rsp];
        return resolve(results);
        //return paginationHelper(resolve, reject, results, rsp.next_page);
      }).catch(err => {
        return reject(err);
      });
    });
};

/*
* Get the number of opted-in Push users who access the app within the specified 
* time period.
* Mandatory: start, end, precision
*/
getOptinsReport = (body, queryString) => {
    return new Promise((resolve, reject) => {
      getReportBase(getURI('/api/reports/optins'), null, queryString)
      .then(rsp => {
        let results = [rsp];
        return resolve(results);
        //return paginationHelper(resolve, reject, results, rsp.next_page);
      }).catch(err => {
        return reject(err);
      });
    });
};

/*
* Get the number of opted-out Push users who access the app within the specified
* time period.
* Mandatory: start, end, precision
*/
getOptoutsReport = (body, queryString) => {
    return new Promise((resolve, reject) => {
      getReportBase(getURI('/api/reports/optouts'), null, queryString)
      .then(rsp => {
        let results = [rsp];
        return resolve(results);
        //return paginationHelper(resolve, reject, results, rsp.next_page);
      }).catch(err => {
        return reject(err);
      });
    });
};

/*
* Get the number of direct and influenced opens of your app.
* Mandatory: start, end, precision
*/
getResponsesReport = (body, queryString) => {
    return new Promise((resolve, reject) => {
      getReportBase(getURI('/api/reports/responses'), null, queryString)
      .then(rsp => {
        let results = [rsp];
        return resolve(results);
        //return paginationHelper(resolve, reject, results, rsp.next_page);
      }).catch(err => {
        return reject(err);
      });
    });
};

/*
* Get the number of direct and influenced opens of your app.
* Mandatory: start, end
* Optional: limit
*/
getResponsesListReport = (body, queryString) => {
    return new Promise((resolve, reject) => {
      getReportBase(getURI('/api/reports/responses/list'), null, queryString)
      .then(rsp => {
        let results = [rsp];
        return resolve(results);
        //return paginationHelper(resolve, reject, results, rsp.next_page);
      }).catch(err => {
        return reject(err);
      });
    });
};

/*
* Returns detailed reports information about a specific push notification. 
* Use the push_id, which is the identifier returned by the API that represents a
* specific push message delivery.
* Mandatory: push_id
*/
getIndividualResponsesReport = (body, queryString) => {
    let push_id = queryString.push_id;
    return getReportBase(getURI(`/api/reports/responses/${push_id}`), 
                         null, null);
};

/*
* Get the number of pushes you have sent within a specified time period.
* Mandatory: start, end, precision
*/
getSendsReport = (body, queryString) => {
    return new Promise((resolve, reject) => {
      getReportBase(getURI('/api/reports/sends'), null, queryString)
      .then(rsp => {
        let results = [rsp];
        return resolve(results);
        //return paginationHelper(resolve, reject, results, rsp.next_page);
      }).catch(err => {
        return reject(err);
      });
    });
};

/*
* Get the average amount of time users have spent in your app within the 
* specified time period.
* Mandatory: start, end, precision
*/
getTimeInAppReport = (body, queryString) => {
    return new Promise((resolve, reject) => {
      getReportBase(getURI('/api/reports/timeinapp'), null, queryString)
      .then(rsp => {
        let results = [rsp];
        return resolve(results);
        //return paginationHelper(resolve, reject, results, rsp.next_page);
      }).catch(err => {
        return reject(err);
      });
    });
};

getWebInteractionsReport = (body, queryString) => {
    return new Promise((resolve, reject) => {
      getReportBase(getURI('/api/reports/web/interaction'), null, queryString)
      .then(rsp => {
        let results = [rsp];
        return resolve(results);
        //return paginationHelper(resolve, reject, results, rsp.next_page);
      }).catch(err => {
        return reject(err);
      });
    });
};



const definition = {
  devices: getDevicesReport,
  events: getEventsReport,
  experiment_overview: getExperimentsOverview,
  opens: getOpensReport,
  optins: getOptinsReport,
  optouts: getOptoutsReport,
  responses: getResponsesReport,
  responses_list: getResponsesListReport,
  individual_responses: getIndividualResponsesReport,
  sends: getSendsReport,
  timeinapp: getTimeInAppReport,
  web_interactions: getWebInteractionsReport  
};

exports.fetchData = function (event, callback) {
  console.log("Start Lambda");
    eventData = event;
    config = eventData.config;    
    wsse = new WSSE(config.credentials.appKey, config.credentials.secretKey);
    urlBase = config.app.baseUrl;
    promises = config.app.reports
      .filter(report => definition.hasOwnProperty(report.name))
      .map(report => {        
          report.params.start = moment(Number(eventData.start_time)).format("YYYY-MM-DD")
          report.params.end = moment(Number(eventData.end_time)).format("YYYY-MM-DD")
          return definition[report.name](report.body, report.params);        
    });
           
    Promise.all(promises).then(rsps => {
      console.log(JSON.stringify(rsps));
      if (rsps.length === 0)
      {
        return callback(null, "There is no data to send to kinesis");
      }      
     return callback(null, rsps);     
    }).catch(err => {
      return callback(err, null);
    });
};

