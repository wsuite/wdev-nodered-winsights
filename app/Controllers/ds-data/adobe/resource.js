const rp = require('request-promise')
const CryptoJS = require('crypto-js')
const fs = require('fs')
var AWS = require('aws-sdk')
var lambda = new AWS.Lambda({ apiVersion: '2015-03-31' })
var sqs = new AWS.SQS({ apiVersion: '2015-03-31' });
var moment = require('moment')
const SQS_QUEUE = process.env.QUEUE_NAME || 'https://sqs.us-east-1.amazonaws.com/133013689155/w-insights-ds-queue-stg'

/* Args:
  {
    "suites": "Optional, Array of strings"
    "suitesBlackList": "Optional, Array of strings",
    "metricsNameWhiteList": "Desired metrics name the script is going to request the data",
    "metricsIdBlackList": "Second filter for available metrics"
  }
*/
/**
  Process description:
  1- Get available suites `getSuites` then filter the response using
     suitesBlackList if it is provided
  2- Get available metrics per suite using `getMetrics(suite.rsid)` then filter 
     the response with the provided metricsNameWhiteList and metricsIdBlackList
  3- Generate a report using `getMetricsData(suiteId, from, to, granularity, metrics, elements)`
     where `from` and `to` are dates in format `YYYY-M-D` for example `2018-4-15`
     and provided in the fields fromDate and toDate, granularity is the time 
     step provided in the granularity field, metrics is the result of the step 
     #2, finally elements are the data to get for the metrics provided in the 
     field metricElements
  4- Get the reports using the `getReportData()`
     
*/

let errors = 0

var uuid = function() {}

uuid.v4 = function() {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
        var r = Math.random() * 16 | 0,
            v = c === 'x' ? r : (r & 0x3 | 0x8)
        return v.toString(16)
    })
};

var WSSE = function(username, secret) {
    this.username = username
    this.secret = secret
};

WSSE.prototype.getHeader = function() {
    var nonce = uuid.v4()
    var created = new Date().toISOString()

    var hashString = nonce + created + this.secret
    var digest = CryptoJS.SHA256(hashString).toString(CryptoJS.enc.Base64)
    var b64nonce = CryptoJS.enc.Latin1.parse(nonce).toString(CryptoJS.enc.Base64)

    var header = "UsernameToken"
    header += " Username=\"" + this.username + "\","
    header += " PasswordDigest=\"" + digest + "\","
    header += " Nonce=\"" + b64nonce + "\","
    header += " Created=\"" + created + "\","
    header += " Algorithm=\"SHA256\""

    return { 'X-WSSE': header }
};

const urlBase = 'https://api.omniture.com/admin/1.4/rest/?method='
const paramsBase = {
    method: 'POST',
    json: true
}

getSuites = (config) => {
    console.log("Get suites")
    let params = Object.assign({
        uri: `${urlBase}Company.GetReportSuites`,
        headers: wsse.getHeader(),
    }, paramsBase)
    return rp(params)
};

getMetrics = (suiteId, config) => {
    let params = Object.assign({
        uri: `${urlBase}Report.GetMetrics`,
        body: {
            reportSuiteID: suiteId,
        },
        headers: wsse.getHeader(),
    }, paramsBase)
    return rp(params)
};

var suites
var cnt = 0
var reports = []
var reportData = []
getData = (callback, suites, cnt, reports, reportData) => {
    const granularity = config.granularity
    const items = config.metricsNameWhiteList.map(a => a.name) || []
    const blacklist = config.metricsIdBlackList || []
    if (cnt >= suites.length) {
        cnt = 0
        return getReportData(callback, cnt, reports, reportData)
    }
    var suite = suites[cnt]
    cnt++
    console.log('Reading from', suite.rsid)
        // const from = moment(Number(eventData.start_time)).format("YYYY-MM-DD")
    const from = moment(Number(eventData.end_time)).subtract(1, "days").format("YYYY-MM-DD")
    const to = moment(Number(eventData.end_time)).format("YYYY-MM-DD")
    getMetricsData(suite.rsid, from, to, granularity, config.metricsNameWhiteList, config.elements)
        .then((res) => {
            if (res) {
                console.log('report: ', res.reportID)
                reports.push({ id: res.reportID, name: suite.rsid })
            }
            getData(callback, suites, cnt, reports, reportData)
        }, (error) => {
            getData(callback, suites, cnt, reports, reportData)
                // console.log(error);
        })
};

getReportData = (callback, cnt, reports, reportData) => {
    console.log('Report length: ')
    console.log(reports.length)
    if (cnt >= reports.length) {
        if (errors > 0) {
            return callback(errors, null)
        } else {
            if (reportData.length > 0) {
                reportData.shift()
                    // Return only single row
                console.log(reportData[0])
                return callback(null, reportData[0])
            } else {
                return callback('No data available', null)
            }

        }
        // return console.log('done report...');
    } else {
        var report = reports[cnt]
        cnt++
        console.log('Reading from report', report)
        getMetricsRawData(report.id)
            .then((data) => {
                reportData[cnt] = data
                getReportData(callback, cnt, reports, reportData)
            })
            .catch(() => {
                console.log('Waiting for report to be ready')
                    // cnt--
                    // setTimeout(() => {
                    //     getReportData(callback, cnt, reports, reportData)
                    // }, 15000)
                var params = {
                    MessageBody: JSON.stringify({ report_id: report.id, ds_name: eventData.ds_name, config: config, analysis_id: eventData.analysis_id, type: 'sample' }),
                    QueueUrl: SQS_QUEUE,
                    DelaySeconds: 20
                }
                sqs.sendMessage(params, function(err, data) {
                    if (err) {
                        return callback('Error while getting data', null)
                    } else {
                        return callback(null, 'in_progress')
                    }
                });
            })
    }
}

getMetricsRawData = (reportId) => {
    let params = Object.assign({
        uri: `${urlBase}Report.Get`,
        body: {
            reportID: reportId
        },
        headers: wsse.getHeader()
    }, paramsBase)
    return rp(params)
};

getMetricsData = (suiteId, from, to, granularity, metrics, elements) => {
    let params = Object.assign({
        uri: `${urlBase}Report.Queue`,
        body: {
            reportDescription: {
                reportSuiteID: suiteId,
                dateFrom: from,
                dateTo: to,
                dateGranularity: granularity,
                metrics: metrics,
                elements: elements
            }
        },
        headers: wsse.getHeader()
    }, paramsBase)
    console.log(params)
    return rp(params)
};

exports.fetchData = function(event, callback) {
    var suites
    var cnt = 0
    var reports = []
    var reportData = []
    eventData = event
    config = event.config
    wsse = new WSSE(config.username, config.secret)
    suites = [{ rsid: config.report_suite_id[0], site_title: 'Report Suite' }]
    getData(callback, suites, cnt, reports, reportData)
};