
var callback = null;
const rp = require('request-promise');
const {google} = require('googleapis');
//const urlBase = 'https://www.googleapis.com/drive/v3/files?q=mimeType="application/vnd.google-apps.spreadsheet"'
const urlBase = 'https://sheets.googleapis.com/v4/spreadsheets/'
const paramsBase = {
    method: 'GET',
    json: true
}

/*
private readonly API_URL: string = 'https://sheets.googleapis.com/v4/spreadsheets';


public findById(spreadsheetId: string, authtoken: string): Observable<any> {
    return this.httpClient.get(this.API_URL + '/' + spreadsheetId, {
        headers: new HttpHeaders({
            Authorization: `Bearer ${authtoken}`
        })
    });
}


public findSheetTabData(spreadsheetId: string, tabName: string, authtoken: string): Observable<any> {
    return this.httpClient.get(this.API_URL + '/' + spreadsheetId + '/values/'+tabName+'!A1:Z1000', {
      headers: new HttpHeaders({
            Authorization: `Bearer ${authtoken}`
        })
    });
}

public getdriveFilelist(authtoken: string): Observable<any> {
    return this.httpClient.get('https://www.googleapis.com/drive/v3/files?q=mimeType="application/vnd.google-apps.spreadsheet"', {
        headers: new HttpHeaders({
            Authorization: `Bearer ${authtoken}`
        })
    });
}
*/


  
  
  



  refreshToken = (config,callabck) => {
    const oauth2Client = new google.auth.OAuth2(
        '551225410189-n46amgivjf8cu7i8d97uol9mvpdstvf5.apps.googleusercontent.com',
        'jMnOHIVmIDOxAHJ3LyX9InDG',
        'http://localhost:4200'
      );
      
      const scopes = [
          "https://www.googleapis.com/auth/userinfo.profile","https://www.googleapis.com/auth/drive","https://www.googleapis.com/auth/drive.file","https://www.googleapis.com/auth/drive.readonly","https://www.googleapis.com/auth/spreadsheets","https://www.googleapis.com/auth/spreadsheets.readonly"
      ]
        const url = oauth2Client.generateAuthUrl({
          // 'online' (default) or 'offline' (gets refresh_token)
          access_type: 'offline',  
          // If you only need one scope you can pass it as a string
          scope: scopes
      });   

    //console.log('config===',config)
    oauth2Client.getToken(config.code);
    
    // oauth2Client.setCredentials(tokens);
    oauth2Client.on('tokens', (tokens) => {
        //console.log('tokens token', oauth2Client.getToken(config.code))
        if (tokens.refresh_token) {
            // store the refresh_token in my database!            
            oauth2Client.setCredentials({
                refresh_token: tokens.refresh_token
            });
            callabck(tokens)
        }else{
            callabck(tokens)
        }
    //console.log('Access token1 : ', tokens.access_token);
    });
  }  

getFiles = (config) => {    
    let params = Object.assign({
        uri: `${urlBase}${config.spreadsheetid}/values/${config.tab_name}!A:ZZ`,
        headers: { 'Authorization': `Bearer ${config.token}` },
    }, paramsBase)
    return rp(params)
};

exports.fetchData = function (event, cb) {    
    callback = cb;

    getFiles(event).then((result) => {
        callback(null,JSON.stringify(result.values));
    }).catch((err) => {
        callback(err,null);
    });
    
   
    refreshToken(event, function (data){
        console.log("refreshToken",data)
    });
    
};
