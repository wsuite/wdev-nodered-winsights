const AWS = require('aws-sdk');
const athena = new AWS.Athena();
const s3 = new AWS.S3();

module.exports = class AthenaController {
    constructor() { }

    static async runquery(event) {
        let eventData = event.queryParam
        let headerData = JSON.parse(event.headers.extraparams)
        let environment = process.env.ENV === 'production' ? '' : `${process.env.ENV}_`
        let params = {
            QueryString: eventData.query,
            QueryExecutionContext: {
                Database: `${environment}${headerData.wsuiteid}_db`
            },
            WorkGroup: `${process.env.ENV}_${headerData.partner}_workgroup`
        };
        const execRes = await athena.startQueryExecution(params).promise()
        return Promise.resolve({ error: false, data: execRes, message: 'Successfully executed' })
    }

    static async status(event) {
        let eventData = event.queryParam
        var params = {
            QueryExecutionId: eventData.queryExecutionId
        };
        let resData = await athena.getQueryExecution(params).promise()
        let dataStatus = {};
        dataStatus.status = resData.QueryExecution.Status.State;
        return Promise.resolve({ error: false, 'message': `Your query is on ${dataStatus.status} state.`, data: dataStatus })
    }

    static async stop(event) {
        let eventData = event.payload.requestData
        var params = {
            QueryExecutionId: eventData.queryExecutionId
        };
        await athena.stopQueryExecution(params).promise()
        return Promise.resolve({ error: false, 'message': `Successfully stopped.`, data: null })
    }

    static async download(event) {
        let headerData = JSON.parse(event.headers.extraparams)
        if(event.payload.requestData && event.payload.requestData.queryExecutionId) {
            let eventData = event.payload.requestData
            // TODO
            let url = s3.getSignedUrl('getObject', {
                Bucket: `${process.env.ENV}-static.wsuite.com`,
                Key: `${headerData.wsuiteid}/insights/query_results/${eventData.queryExecutionId}.csv`,
                Expires: 60 * 5
            })
            return Promise.resolve({ error: false, 'message': `Successfully generated.`, data: url })
        }
    }

    static async truncate(event) {
        let eventData = event.payload.requestData
        let headerData = JSON.parse(event.headers.extraparams)
        // TODO
        let params = {
            Bucket: `${process.env.ENV}-static.wsuite.com`,
            Prefix: `${headerData.wsuiteid}/insights/data_warehouse/${eventData.clientid}/${eventData.table_name}`
        };
        let data = await s3.listObjects(params).promise()
        if (data.Contents.length == 0)
            return Promise.resolve({ error: false, 'message': `No Data`, data: {} })
        
        let s3params = { Bucket: `${process.env.ENV}-static.wsuite.com` };
        s3params.Delete = { Objects: [] };
        
        data.Contents.forEach(function (content) {
            s3params.Delete.Objects.push({ Key: content.Key });
        });

        let deletedData = await s3.deleteObjects(s3params).promise()
        if (deletedData.Contents && deletedData.Contents.length == 1000) {
            return await this.truncate(event)
        }
        return Promise.resolve({ error: false, 'message': `Done`, data: {} })
    }

    static async result(event) {
        let eventData = event.queryParam
        var params = {
            QueryExecutionId: eventData.queryExecutionId
        };
        let resData = await athena.getQueryResults(params).promise()
        let Rows = resData.ResultSet.Rows;
        //For Columns
        let Columns = Rows[0].Data;
        let arrCol = [];
        Columns.forEach(function (element, rowKey) {
            arrCol.push(element.VarCharValue);
        });

        //For Values
        var finalResult = [];
        Rows.forEach(function (element, rowKey) {
            if (rowKey != 0) {
                var rowData = element.Data;
                let arrValue = [];
                rowData.forEach(function (valObj, key) {
                    arrValue.push(valObj.VarCharValue)
                });
                var i, keys = arrCol, values = arrValue;
                var tmp = {};
                for (i = 0; i < keys.length; i++) {
                    tmp[keys[i]] = values[i];
                }
                finalResult.push(tmp);
            }
        });
        return Promise.resolve({ error: false, 'message': null, data: finalResult })
    }
}