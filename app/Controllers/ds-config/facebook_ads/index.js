var access_token, eventData;

const https = require('https');
const rp = require('request-promise');



var datasource = async function (event) {
    try {
        let options = {
            method: 'GET',
            uri: `https://graph.facebook.com/me?access_token=${event.access_token}`
        };
        let data = await rp(options)
        return data
    } catch (error) {
        Promise.reject(error)
    }
    
    // access_token = event.access_token;
    // eventData = event
    // https.get('https://graph.facebook.com/me?access_token=' + access_token, (resp) => {
    //     let data = '';

    //     // A chunk of data has been recieved.
    //     resp.on('data', (chunk) => {
    //         data += chunk;
    //     });

    //     // The whole response has been received. Print out the result.
    //     resp.on('end', () => {
    //         data = JSON.parse(data);
    //         if (data.error) {
    //             cb({
    //                 'success': false,
    //                 'message': 'token is not working!',
    //                 'data': data
    //             }, null);
    //         }
    //         else {
    //             cb(null, {
    //                 'success' : true,        
    //                 'message': 'token is working!',
    //                 'data': data
    //             });
    //         }
    //     });
    // });
}

module.exports = datasource;