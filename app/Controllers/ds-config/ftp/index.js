var PromiseFtp = require('promise-ftp');

var datasource = async (event) => {
	try {
		var ftp = new PromiseFtp();
		var connectionProperties = {
			host: event.host,
			user: event.username,
			password: event.password	
		};

		await ftp.connect(connectionProperties);
		return 'Done'
	} catch (error) {
		console.log(error)
		return Promise.reject('Error while connecting to FTP. Please check your details')
	}
}

module.exports = datasource;