// modules
const rp = require('request-promise');

let config = {};
let eventData = {};
let context = null;
let wsse = null;
let urlBase = null;

const paramsBase = {
    method: 'GET',
    json: true,
};

const WSSE = function (username, secret) {
    this.username = username;
    this.secret = secret;
};

WSSE.prototype.getHeader = function () {
    let auth = [this.username, this.secret].join(':');
    let buffAuth = new Buffer(auth);
    let base64Auth = buffAuth.toString('base64');
    return { 'Authorization': 'Basic ' + base64Auth };
};

getProfiles = (event) => {
    urlBase = event.url;
    const params = Object.assign(paramsBase, {
        uri: `${urlBase}profiles`,
        headers: wsse.getHeader(),
    });
    return rp(params);
};

var datasource = async (event) => {
    try {
        eventData = event
        wsse = new WSSE(event.client, event.password);
        urlBase = event.url;
        let profileData = await getProfiles(event)
        return profileData.profiles
    } catch (error) {
        console.log(error)
        return Promise.reject('Error while getting profiles from Socialbaker')
    }
    
}
module.exports = datasource;