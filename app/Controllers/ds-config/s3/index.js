var AWS = require('aws-sdk');
var s3, sub_type, eventData;
var datasource = async function (event) {
    sub_type = event.sub_type;
    eventData = event
    s3 = new AWS.S3({ accessKeyId: eventData.awsaccesskeyid, secretAccessKey: eventData.awssecretaccesskey });
    try {
        if (sub_type == 'file') {
            await isFileExits()
            return { 'success': true }
        } else if (sub_type == 'folder') {
            await isFolderExits()
            return { 'success': true }
        }    
    } catch (error) {
        return Promise.reject('Wrong credentials. Please check your credentials again.')
    }
    
}

isFileExits = async function () {
    var params = {
        Bucket: eventData.bucket,
        Key: eventData.filepath
    };
    return await s3.headObject(params).promise();
}

isFolderExits = async function () {
    var params = {
        Bucket: eventData.bucket,
        MaxKeys: 10,
        Prefix: eventData.folderpath
    };
    let folderData = await s3.listObjectsV2(params).promise()
    if (folderData.Contents && folderData.Contents.length == 0) {
       return Promise.resolve('no_data')
    } else {
        return Promise.resolve(folderData)
    }
}

module.exports = datasource;