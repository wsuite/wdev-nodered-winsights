const rp = require('request-promise');

const paramsBase = {
    method: 'GET',
    json: true
}

let uri = "https://api.parrotanalytics.com/latest/demand";

const WSSE = function (apiKey) {
    this.apiKey = apiKey;
};

WSSE.prototype.getHeader = function () {
    return { 'X-API-KEY': this.apiKey };
};

function getFinalParams(wsse, queryString) {
    let params = Object.assign(paramsBase, {
        uri: uri,
        headers: wsse.getHeader(),
    });

    if (queryString)
        params.qs = queryString;

    return params;
}

var datasource = async (event) => {
    wsse = new WSSE(event.apiKey);
    var queryParams = {
        date_from: event.date_from,
        date_to: event.date_to,
        markets_iso: event.markets_iso,
        content_ids: event.content_ids,
        metric_type: event.metric_type,
        interval: event.interval
    };

    if (getDateIncrement(event) == "daily") {
        queryParams.date_to = queryParams.date_from;
    }

    params = getFinalParams(wsse, queryParams);
    try {
        let rsps = await rp(params)
        if(rsps.error) {
            return Promise.reject(rsps.error)
        } else if (rsps.message) {
            return Promise.reject(rsps.message)
        } else if (rsps.metric) {
            return JSON.stringify(rsps)
        } else {
            return Promise.reject('Error while getting records, please check parameters')
        }
    } catch (error) {
        console.log(error)
        return Promise.reject('Error while getting records, please check parameters')
    }
    

    function getDateIncrement(params) {
        if (params.interval == "daily") {
            return "daily";
        } else if (params.interval == "weekly") {
            return "weekly";
        } else if (params.interval == "monthly") {
            return "monthly";
        } else if (params.interval == "overall") {
            if (params.content_ids == "topshows") {
                return "weekly";
            } else {
                return "daily";
            }
        } else {
            return "daily";
        }
    }
};

module.exports = datasource;