var Twitter = require('twitter');
var datasource = async (event) => {
	try {
		var access_token_key= event.access_token_key; 
		var access_token_secret = event.access_token_secret;
		var consumer_key= event.consumer_key;
		var consumer_secret= event.consumer_secret;

		var client = new Twitter({
		consumer_key: consumer_key,
		consumer_secret: consumer_secret,
		access_token_secret: access_token_secret
		});

		var params = {screen_name: event.screen_name, count : event.count};	
		return client.get('statuses/user_timeline', params)
	} catch (error) {
		console.log(error)
		return Promise.reject(error)
	}
    

}

module.exports = datasource;