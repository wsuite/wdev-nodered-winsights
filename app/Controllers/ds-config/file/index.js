const AWS = require('aws-sdk')
const s3 = new AWS.S3()
const awsAccessKeyId = process.env.AWSACCESSKEYID
const awsSecretAccessKey = process.env.AWSSECRETACCESSKEY
AWS.config.update({ accessKeyId: awsAccessKeyId, secretAccessKey: awsSecretAccessKey, region: 'us-east-1' })

var datasource = async (event, headers) => {
	// TODO
	try {
		const myBucket = `${process.env.ENV}-static.wsuite.com`
		const myKey = `${headers.wsuiteid}/insights/${headers.brandid}/files/${event.key}`
		var params = {
			Bucket: myBucket,
			Fields: {
				key: myKey
			}
		};
		let data = s3.createPresignedPost(params);
		return data.fields
	} catch (error) {
		console.log(error)
		return Promise.reject('Error while generating presign URL')
	}

}


module.exports = datasource;