const { Pool, Client } = require('pg')

var datasource = async (event) => {	
	try {
		// TODO
		const env_host = process.env.ERP_HOST
		const env_username = process.env.ERP_USERNAME
		const env_password = process.env.ERP_PASSWORD
		var pool = new Pool({
			user: env_username,
			host: env_host,
			database: `werp_${event.database}`,
			password: env_password
		})
		
		let data = await pool.query('SELECT table_name FROM information_schema.tables WHERE table_type=\'BASE TABLE\' AND table_schema=\'public\'')
		let dbdata = await pool.query('SELECT datname FROM pg_database WHERE datistemplate = false')
		await pool.end()
		if(event.subType == 'gettable')	{
			return data.rows
		} else if (event.subType === 'getdatabase') {
			return dbdata.rows
		} else {
			return true
		}
	} catch (error) {
		console.log(event)
		console.log(error)
		return Promise.reject('Error while connecting to Database')
	}
}

module.exports = datasource;