var AWS = require('aws-sdk');
var Facebook = require('fb').Facebook,
    FB = new Facebook({
        Promise: require('bluebird')
    });


// JSON-like object containing the event parameters passed in to the lambda 
// function
let config = {};
let eventData = {};

// Object containing lambda specific context properties and methods
let callback = null;

getToken = async (config) => {
    let data = await FB.api('oauth/access_token', {
        client_id: config.facebookclientid,
        client_secret: config.facebooksecretkey,
        grant_type: 'client_credentials'
    })
    return data
};

module.exports = function (event) {
    eventData = event
    config = event;
    return getToken(config);
};