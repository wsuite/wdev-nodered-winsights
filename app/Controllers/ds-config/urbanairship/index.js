const rp = require('request-promise');
const path = require('path');
var moment = require('moment');
var kinesis;

let config = {};

let eventData = {};

let context = null;

let wsse = null;

//let urlBase = null;

const paramsBase = {
  method: 'GET',
  json: true,
};

const WSSE = function (appKey, appSecret) {
  this.appKey = appKey;
  this.appSecret = appSecret;
};

WSSE.prototype.getHeader = function () {
  let auth = [this.appKey, this.appSecret].join(':');
  let buffAuth = new Buffer(auth);
  let base64Auth = buffAuth.toString('base64');
  return { 'Authorization': 'Basic ' + base64Auth };
};

getURI = (url) => {
  return `https://go.urbanairship.com${url}`;
};

getReportBase = (uri, body, queryString) => {
  let params = Object.assign(paramsBase, {
    uri: uri,
    headers: wsse.getHeader(),
  });

  if (body)
    params.body = body;

  if (queryString)
    params.qs = queryString;

  return rp(params);
};
var getDevicesReport = (queryString) => {
  return getReportBase(getURI('/api/reports/devices'), null, queryString);
};

var datasource = async (event) => {
  try {
    wsse = new WSSE(event.appKey, event.secretKey);
    var params = { "date": event.date };
    var devicedetails = getDevicesReport(params);
    promises = [devicedetails]
    let rsps = await Promise.all(promises)
    if (rsps[0].total_unique_devices) {
      return JSON.stringify(rsps[0])
    }
    return Promise.reject('Error while getting data')
  } catch (error) {
    return Promise.reject('Error while getting data')
  }

};
module.exports = datasource;