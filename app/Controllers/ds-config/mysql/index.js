var mysql = require('promise-mysql');

var datasource = async (event) => {
	console.log(event)
	try {
		const client = await mysql.createConnection({
			host: event.host,
			user: event.user,
			password: event.password
		});
		let data = {}
		if (event.subType == 'gettable') {
			data = await client.query('select table_name from information_schema.tables WHERE TABLE_SCHEMA = ?', [event.database])
		}
		await client.destroy()
		return data
	} catch (error) {
		console.log(error)
		return Promise.reject('Error while connecing to mysql database')
	}
	
}


//Get TableName Query :
//SELECT table_name FROM information_schema.tables WHERE table_type='BASE TABLE' AND table_schema='public'; 

module.exports = datasource;