let eventData = {};
var { google } = require('googleapis');

var datasource = function (event, cb) {
    eventData = event;
   
    var private_key = eventData.private_key.replace(new RegExp("<br>", "g"),"\n").replace(new RegExp("<plus>", "g"),"+");
    var client_email = eventData.client_email.trim();
    console.log(private_key);
    console.log(client_email);
    try {
        var jwtClient = new google.auth.JWT(client_email, // For authenticating and permissions
            null,
            private_key,
            ['https://www.googleapis.com/auth/analytics.readonly'],
            null);
        await jwtClient.authorize()
        return 'Done'
    } catch (error) {
        return Promise.reject('Invalid configurations. Please check again.')
    }
} 

module.exports = datasource;

