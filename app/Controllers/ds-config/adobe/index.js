const rp = require('request-promise');
const CryptoJS = require('crypto-js');

let errors = 0;

var uuid = function () {
};

uuid.v4 = function () {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
        var r = Math.random() * 16 | 0, v = c === 'x' ? r : (r & 0x3 | 0x8);
        return v.toString(16);
    });
};

var WSSE = function (username, secret) {
    this.username = username;
    this.secret = secret;
};

WSSE.prototype.getHeader = function () {
    var nonce = uuid.v4();
    var created = new Date().toISOString();

    var hashString = nonce + created + this.secret;
    var digest = CryptoJS.SHA256(hashString).toString(CryptoJS.enc.Base64);
    var b64nonce = CryptoJS.enc.Latin1.parse(nonce).toString(CryptoJS.enc.Base64);

    var header = "UsernameToken";
    header += " Username=\"" + this.username + "\",";
    header += " PasswordDigest=\"" + digest + "\",";
    header += " Nonce=\"" + b64nonce + "\",";
    header += " Created=\"" + created + "\",";
    header += " Algorithm=\"SHA256\"";

    return {'X-WSSE': header};
};

// const urlBase = 'https://api.omniture.com/admin/1.4/rest/?method=';
var urlBase = "";
const paramsBase = {
    method: 'POST',
    json: true
};

getSuites = (event) => {
    urlBase = event.url;
    let params = Object.assign({
        uri: `${urlBase}Company.GetReportSuites`,
        headers: wsse.getHeader(),
    }, paramsBase);
    return rp(params);
};

getMetrics = (event) => {
    urlBase = event.url;
    let params = Object.assign({
        uri: `${urlBase}Report.GetMetrics`,
        headers: wsse.getHeader(),
        body: {
            reportSuiteID: event.suite_id
        },
    }, paramsBase);
    return rp(params);
}

getElements = (event) => {
    urlBase = event.url;
    let params = Object.assign({
        uri: `${urlBase}Report.GetElements`,
        headers: wsse.getHeader(),
        body: {
            reportSuiteID: event.suite_id
        },
    }, paramsBase);
    return rp(params);
}

var datasource = async function(event, cb) {
    wsse = new WSSE(event.username, event.secret);
    if(event.subtype == 'metrics') {
        try {
            return await getMetrics(event)
        } catch (error) {
            return Promise.reject(error)
        }
    } else if(event.subtype == 'elements') {
        try {
            return await getElements(event)
        } catch (error) {
            return Promise.reject(error)
        }
    } else {
        try {
            let data = await getSuites(event)
            return data['report_suites']
        } catch (error) {
            return Promise.reject(error)
        }
    }
}
module.exports = datasource;