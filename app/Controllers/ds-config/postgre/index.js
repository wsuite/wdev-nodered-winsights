const { Pool, Client } = require('pg')
var rp = require('request-promise')

var datasource = async (event) => {	
		try {
			var pool = new Pool({
				user: event.user,
				host: event.host,
				database: event.database,
				password: event.password
			})
	
			let data = await pool.query('SELECT table_name FROM information_schema.tables WHERE table_type=\'BASE TABLE\' AND table_schema=\'public\'')
			await pool.end()
			if(event.subType == 'gettable')	{
				return data.rows
			} else {
				return true
			}
		} catch (error) {
			console.log(error)
			return Promise.reject('Error while connecting to Database')
		}
}


//Get TableName Query :
//SELECT table_name FROM information_schema.tables WHERE table_type='BASE TABLE' AND table_schema='public'; 

module.exports = datasource;