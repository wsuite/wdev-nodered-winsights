const AWS = require('aws-sdk');
var async = require('async')
var moment = require('moment')
var uuidv4 = require('uuid/v4')
var dynamodb = new AWS.DynamoDB()
var lambda = new AWS.Lambda()
var cloudwatchevents = new AWS.CloudWatchEvents()
var ecs = new AWS.ECS()
var s3 = new AWS.S3()
var glue = new AWS.Glue()
var athena = new AWS.Athena()
var taskDef

const ECS_CLUSTER = `${process.env.CLUSTER}`
const ECS_SUBNET1 = `${process.env.SUBNET1}`
const ECS_SUBNET2 = `${process.env.SUBNET2}`
const ECS_SG = `${process.env.SG}`

const dynamodb_ds_table = `${process.env.ENV}-winsights-datasources`
const dynamodb_ds_history_table = `${process.env.ENV}-datasource-run-history`
const cloudwatch_event_role = 'arn:aws:iam::133013689155:role/CloudWatchEventRole'
const trigger_lambda = 'arn:aws:lambda:us-east-1:133013689155:function:winsights-ds-trigger-lambda'
const job_trigger_lambda = 'arn:aws:lambda:us-east-1:133013689155:function:analysis-trigger-lambda'
const ds_lambdas = {
    adobe: 'winsights-adobe-ds-lambda',
    facebook2: 'winsights-facebook-ds-lambda', //changed to facebook2 for making room for facebook, so to conitnue with current code
    socialbaker: 'winsights-socialbaker-ds-lambda',
    urbanairship: 'winsights-urbanairship-ds-lambda',
    googleanalytics: 'winsights-googleanalytics-ds-lambda',
    twitter: 'winsights-twitter-ds-lambda',
    facebook: 'winsights-facebookads-ds-lambda',
    youtube: 'winsights-youtube-ds-lambda',
    exacttarget: 'winsights-python-datasource-data-provider-stg',
    parrot: 'winsights-parrot-ds-lambda',
    postgre: 'winsights-postgre-ds-lambda',
    werp: 'winsights-postgre-ds-lambda',
    mysql: 'winsights-mysql-ds-lambda',
    wproject: 'winsights-wproject-ds-lambda',
    wpeople: 'winsights-wproject-ds-lambda',
    wplan: 'winsights-wsuite-ds-lambda',
    wtrack: 'winsights-wsuite-ds-lambda',
    instagram: 'winsights-instagram-ds-lambda',
    table: 'analysis-trigger-lambda'
}
const ds_fargate = {
    s3: {
        file: `${process.env.ENV}-winsights-s3-datasource-docker`,
        folder:
            `arn:aws:lambda:us-east-1:133013689155:function:${process.env.ENV}-winsights-s3-datasource-handler`
    },
    googlespreadsheet: `${process.env.ENV}-winsights-googlesheet-datasource-docker`,
    ftp: `${process.env.ENV}-winsights-ftp-datasource-docker`,
    ibope: {
        file: `${process.env.ENV}-winsights-s3-datasource-docker`
    },
    rac: {
        file: `${process.env.ENV}-winsights-s3-datasource-docker`
    },
}

module.exports = class DatasourceController {
    constructor() { }

    static async run(event) {
        let postData = event.payload.requestData
        let dsData = await this.getDS(postData.name)
        let inputData = postData.inputs || {}
        let dsType = dsData.ds_type.S
        let run_id
        if(dsType === '') {
            run_id = uuidv4()
            postData.start_time = Date.now()
            await this.createRunId(postData, run_id)
            // If the datasource is not configured
            let jobData = await this.getJobDetails(postData.name)
            await this.runJob(jobData,inputData,run_id)
        }
        return Promise.resolve({ data: { 'run_id': run_id }, message: 'Successfully triggered analysis', error: false })
    }

    static async create(event) {
        let run_id
        let lambdaEvent
        let postData = event.payload.requestData
        let headerData = JSON.parse(event.headers.extraparams)
        let isDsExist = await this.isDatasourceExist(postData.name)
        if (isDsExist) {
            return Promise.resolve({ error: true, data: null, message: `Datasource with name ${postData.name} is already exist.` })
        }
        let res = await this.saveDatasource(postData, headerData)
        if (!postData.ds_type) {
            return Promise.resolve({ data: {}, message: 'Successfully created datasource', error: false })            
        }
        else if (postData.ds_type in ds_lambdas) {
            if (typeof postData.schedule === undefined || !postData.schedule) {
                run_id = uuidv4()
                postData.start_time = Date.now()
                let runData = await this.createRunId(postData, run_id)
                lambdaEvent = {
                    config: JSON.stringify(postData.config),
                    start_time: postData.start_time * 1000,
                    end_time: postData.end_time * 1000,
                    ds_name: postData.name,
                    run_id: run_id
                }
                var params = {
                    FunctionName: `${process.env.ENV}-${ds_lambdas[postData.ds_type]}`,
                    InvocationType: 'Event',
                    Payload: JSON.stringify(lambdaEvent)
                }
                await lambda.invoke(params).promise()
                return Promise.resolve({ data: { 'run_id': run_id }, message: 'Successfully created datasource', error: false })
            } else {
                await this.createScheduleEvent(postData)
                return Promise.resolve({ data: {}, message: 'Successfully created datasource', error: false })
            }
        } else if ((postData.ds_type == 's3' && postData.config.ds_subtype == 'file') || postData.ds_type == 'googlespreadsheet' || postData.ds_type == 'ftp' || postData.ds_type == 'ibope' || postData.ds_type == 'rac') {
            run_id = uuidv4()
            postData.start_time = Date.now()
            await this.createRunId(postData, run_id)
            console.log('Created runid')
            let command
            if (postData.ds_type == 's3' || postData.ds_type == 'ibope' || postData.ds_type == 'rac') {
                taskDef = ds_fargate[postData.ds_type][postData.config.ds_subtype]
                // TODO
                var bucketname = 'winsights-data';
                var pathname = '';
                if (postData.ds_type == 'ibope' || postData.ds_type == 'rac') {
                    bucketname = 'static.wsuite.com';
                    pathname = 'winsights/' + postData.config.filename;
                }
                command = [
                    'python',
                    '/tmp/process.py',
                    postData.name,
                    run_id,
                    postData.config.bucket || bucketname,
                    postData.config.path || pathname
                ]
            } else if (postData.ds_type == 'googlespreadsheet' || postData.ds_type == 'ftp') {
                taskDef = ds_fargate[postData.ds_type]
                command = [
                    'node',
                    '/data/package/script.js',
                    postData.name,
                    run_id
                ]
            }
            console.log(taskDef, postData, command, run_id)
            await this.startTask(taskDef, postData, command, run_id)
            if(postData.schedule) {
                await this.createScheduleEvent(postData)
            }
            return Promise.resolve({ data: { 'run_id': run_id }, message: 'Successfully created datasource', error: false })
        } else if (
            postData.ds_type == 's3' &&
            postData.config.ds_subtype == 'folder'
        ) {
            await this.createS3Event(postData)
            return Promise.resolve({ data: {}, message: 'Successfully created datasource', error: false })
        }
        return Promise.resolve(event.payload.requestData)
    }

    static async getJobDetails(ds_name) {
        var params = {
            Key: {
                "name": {
                    S: ds_name
                }
            },
            TableName: dynamodb_ds_table
        };
        let ds_data = await dynamodb.getItem(params).promise()
        let jobData = await this.getJob(ds_data.Item)
        if (jobData) {
            let code = jobData.code ? jobData.code.S : ''
            return Promise.resolve({ "ds_name": ds_name, "name": jobData.name.S, "order": jobData.order.S, "type": jobData.type.S, "code": code, "table_name": ds_data.Item.table_name.S, "db_name": ds_data.Item.db_name.S, "client_id": ds_data.Item.wclient_id.S, "config": ds_data.Item.config.S, "company_id": ds_data.Item.company_id.S, "company_slug": ds_data.Item.company_slug.S })
        } else {
            return Promise.resolve({ "config": ds_data.Item.config.S });
        }
    }

    static async getNextJob(allJobs, currentJob) {
        existingJob = allJobs.L.filter(function (job) {
            if (job.M.name.S == currentJob) {
                return job;
            }
        });
    
        currentOrder = existingJob[0].M.order.S;
        nextJobArray = allJobs.L.filter(function (job) {
            if (job.M.order.S == parseInt(currentOrder) + 1) {
                return job;
            }
        });
        if (nextJobArray.length > 0) {
            return Promise.resolve(nextJobArray[0].M);
        } else {
            return Promise.resolve(null);
        }
    };

    static async getJob(Item) {
        if (typeof Item.jobs != 'undefined') {
            let jobsFiltered = Item.jobs.L.filter(function (job) {
                if (job.M.order.S == '1') {
                    return job.M.name.S
                }
            });
            if (jobsFiltered.length > 0) {
                return Promise.resolve(jobsFiltered[0].M)
            } else {
                return Promise.resolve(null)
            }
        } else {
            return Promise.resolve(null)
        }
    }

    static async updateJobStatus(run_id, order, job_run_id, status, start_time, job_name, job_type) {
        order = parseInt(order) - 1; // This is because order starts from 1, and our array indexing from 0
        console.log("Updating status for job", order, ":", status);
        var prms = {
            Key: {
                "run_id": {
                    S: run_id
                }
            },
            ExpressionAttributeValues: {
                ":s": {
                    "M": {

                    }
                }
            },
            ReturnValues: "ALL_NEW",
            TableName: dynamodb_ds_history_table,
            UpdateExpression: "SET steps.jobs[" + order + "] = :s"
        }
        // Create an empty object in jobs first
        await dynamodb.updateItem(prms).promise()
        var params = {
            ExpressionAttributeNames: {
                "#S": "status",
                "#st": "execution_start_time",
                "#jr": "job_run_id",
                "#n": "name",
                "#t": "type"
            },
            ExpressionAttributeValues: {
                ":s": {
                    S: status
                },
                ":jr": {
                    S: job_run_id
                },
                ":st": {
                    S: start_time
                },
                ":n": {
                    S: job_name
                },
                ":t": {
                    S: job_type
                }
            },
            Key: {
                "run_id": {
                    S: run_id
                }
            },
            ReturnValues: "ALL_NEW",
            TableName: dynamodb_ds_history_table,
            UpdateExpression: "SET steps.jobs[" + order + "].#S = :s, steps.jobs[" + order + "].#st = :st, steps.jobs[" + order + "].#n = :n, steps.jobs[" + order + "].#t = :t, steps.jobs[" + order + "].#jr = :jr"
        };
        await dynamodb.updateItem(params).promise()
        return Promise.resolve('Done')
    }

    static async getNextJobDetails(datasource, job_name) {
        var params = {
            Key: {
                name: {
                    S: datasource
                }
            },
            TableName: dynamodb_ds_table
        };
        let data = await dynamodb.getItem(params).promise()
        let jobData = await this.getNextJob(data.Item.jobs, job_name)
        if (jobData) {
            code = jobData.code ? jobData.code.S : ''
            return Promise.resolve({ "ds_name": datasource, "name": jobData.name.S, "order": jobData.order.S, "type": jobData.type.S, "code": code, "table_name": data.Item.table_name.S, "db_name": data.Item.db_name.S, "client_id": data.Item.wclient_id.S, "company_id": data.Item.company_id.S, "company_slug": data.Item.company_slug.S });
        } else {
            return Promise.resolve(null);
        }
    };

    static async getDS(datasource) {
        var params = {
            Key: {
                name: {
                    S: datasource
                }
            },
            TableName: dynamodb_ds_table
        };
        let data = await dynamodb.getItem(params).promise()
        return Promise.resolve(data.Item)
    };

    static async runJob(jobData, inputData, run_id) {
        console.log("Running job: " + jobData['name']);
        if (jobData['type'] == 'homologate') {
            var jobParams = {
                JobName: jobData['name'],
                Arguments: {
                    '--run_id': run_id,
                    '--ds_name': jobData['ds_name'],
                    '--table_name': jobData['table_name'],
                    '--path': 's3://winsights-data/warehouse_v2/' + jobData['client_id'] + '/' + jobData['table_name'] + '/',
                    '--company_id': jobData.company_id,
                    '--company_slug': jobData.company_slug
                }
            };
            if(Object.keys(inputData).length > 0) {
                for (const arg in inputData) {
                    jobParams.Arguments[`--${arg}`] = inputData[arg]
                }
            }
            console.log("Starting glue job execution");
            let data = await glue.startJobRun(jobParams).promise()
            let jobRunID = data.JobRunId;
            let execution_start_time = String(Date.now())
            await this.updateJobStatus(run_id, jobData.order, jobRunID, "in_progress", execution_start_time, jobData.name, jobData.type)
            return Promise.resolve('Done')
        } else if (jobData['type'] == 'modelate') {
            var params = {
                QueryString: jobData['code'],
                QueryExecutionContext: {
                    Database: jobData['db_name']
                },
                WorkGroup: `${process.env.ENV}_${jobData.company_slug}_workgroup`
            };
            let data = await athena.startQueryExecution(params).promise()
            let execution_start_time = String(Date.now())
            await this.updateJobStatus(run_id, jobData.order, data.QueryExecutionId, "in_progress", execution_start_time, jobData.name, jobData.type)
            console.log(data.QueryExecutionId);
            setInterval( async function () {
                var _this = this
                let status = await this.getQueryStatus(data.QueryExecutionId)
                if (status == 'SUCCEEDED') {
                    clearInterval(_this)
                    let nextJobData = await this.getNextJobDetails(jobData['ds_name'], jobData['name'])
                    await this.updateJobComplete(run_id, jobData.order, "complete", execution_end_time)
                    if (nextJobData) {
                        let jobResponse = await this.runJob(nextJobData, {}, run_id)
                        status = jobResponse.status || 'jobs_running';
                        return Promise.resolve({ status: status })
                    } else {
                        console.log('NextJob', nextJobData)
                        return Promise.resolve({ status: 'complete' });
                    }
                } else if (status == 'FAILED' || status == 'CANCELLED') {
                    clearInterval(_this)
                    console.log('failed');
                    return Promise.resolve({ status: 'jobs_failed' });
                } else {
                    // Status is RUNNING
                    console.log('Waiting for few seconds..')
                }
            }, 3000);
        }
    }

    static async updateJobComplete(run_id, order, status, end_time) {
        order = parseInt(order) - 1; // This is because order starts from 1, and our array indexing from 0
        console.log("Updating status for job", order, ":", status);
        var params = {
            ExpressionAttributeNames: {
                "#S": "status",
                "#et": "execution_end_time",
            },
            ExpressionAttributeValues: {
                ":s": {
                    S: status
                },
                ":et": {
                    S: end_time
                },
            },
            Key: {
                "run_id": {
                    S: run_id
                }
            },
            ReturnValues: "ALL_NEW",
            TableName: dynamodb_ds_history_table,
            UpdateExpression: "SET steps.jobs[" + order + "].#S = :s, steps.jobs[" + order + "].#et = :et"
        };
        try {
            let data = await dynamodb.updateItem(params).promise()
            return Promise.resolve(data)
        } catch (error) {
            return await handleNoMap();
        }

        async function handleNoMap() {
            var params = {
                ExpressionAttributeNames: {
                    "#S": "status"
                },
                ExpressionAttributeValues: {
                    ":s": {
                        M: {
                            "#S": {
                                S: status
                            },
                            "execution_end_time": {
                                S: end_time
                            }
                        }
                    }
                },
                Key: {
                    "run_id": {
                        S: run_id
                    }
                },
                ReturnValues: "ALL_NEW",
                TableName: dynamodb_ds_history_table,
                UpdateExpression: "SET steps.jobs[" + order + "] = :s"
            };
            await dynamodb.updateItem(params).promise()
        }
    }
    static async getQueryStatus(queryId) {
        var params = {
            QueryExecutionId: queryId
        };
        let data = await athena.getQueryExecution(params).promise()
        return Promise.resolve(data.QueryExecution.Status.State)
    }

    static async createS3Event(eventData) {
        const lambdaParam = {
            Action:        'lambda:InvokeFunction',
            FunctionName:  ds_fargate[eventData.ds_type][eventData.config.ds_subtype],
            Principal:     's3.amazonaws.com',
            StatementId:   Math.random().toString(16).slice(2)
        };
        await lambda.addPermission(lambdaParam).promise()
        let notificationResp = await this.getExistingNotifications(eventData.config.bucket)
        let newEvent = {
            Events: ['s3:ObjectCreated:Put'],
            LambdaFunctionArn: ds_fargate[eventData.ds_type][eventData.config.ds_subtype],
            Filter: {
                Key: {
                    FilterRules: [
                        {
                            Name: 'prefix',
                            Value: eventData.config.prefix_path
                        },
                        {
                            Name: 'suffix',
                            Value: '.csv'
                        }
                    ]
                }
            },
            Id: String(+new Date())
        }
        let notifications = notificationResp.LambdaFunctionConfigurations || []
        notifications.push(newEvent)
        var params = {
            Bucket: eventData.config.bucket,
            NotificationConfiguration: {
                /* required */
                LambdaFunctionConfigurations: notifications
            }
        }
        return await s3.putBucketNotificationConfiguration(params).promise()
    };

    static async getExistingNotifications(bucket) {
        var params = {
            Bucket: bucket
        }
        return s3.getBucketNotificationConfiguration(params).promise()
    }

    static async startTask(taskDef, eventData, commandData, runId) {
        // TODO
        var params = {
            taskDefinition: taskDef,
            cluster: ECS_CLUSTER,
            count: 1,
            group: 'winsights-processes',
            launchType: 'FARGATE',
            networkConfiguration: {
                awsvpcConfiguration: {
                    subnets: [ECS_SUBNET1, ECS_SUBNET2],
                    assignPublicIp: 'DISABLED',
                    securityGroups: [ECS_SG]
                }
            },
            overrides: {
                containerOverrides: [
                    {
                        command: commandData,
                        name: taskDef.replace(`${process.env.ENV}-`,'')
                    }
                ],
            },
            startedBy: 'winsights-ds'
        }
        console.log(JSON.stringify(params))
        return await ecs.runTask(params).promise()
    };

    static async createScheduleEvent(eventData) {
        var params = {
            Name: `${process.env.ENV}-${eventData.name}-event`,
            Description: 'Schedule event for ' + eventData.name,
            // RoleArn: cloudwatch_event_role,
            ScheduleExpression: eventData.schedule,
            State: 'ENABLED'
        }
        await cloudwatchevents.putRule(params).promise()
        var targetParams = {
            Rule: `${process.env.ENV}-${eventData.name}-event`,
            Targets: [
                {
                    Arn: eventData.ds_type == 'table' ? job_trigger_lambda : trigger_lambda,
                    Id: 'cloudwatcheventtarget',
                    Input: `{"current_step":"init", "ds_name":"${eventData.name}'", "lambda":"${process.env.ENV}-${ds_lambdas[eventData.ds_type]}"}`
                }
            ]
        }

        await cloudwatchevents.putTargets(targetParams).promise()
        return Promise.resolve('success')
    };

    static async createRunId(eventData, runId) {
        var start_execution_time = String(Date.now())
        var params = {
            Item: {
                run_id: {
                    S: runId
                },
                name: {
                    S: eventData.name
                },
                status: {
                    S: 'pending'
                },
                total_ingest: {
                    N: '0'
                },
                total_processed: {
                    N: '0'
                },
                created_at: {
                    S: String(+ new Date())
                },
                "execution_start_time": {
                    S: start_execution_time
                },
                "steps": {
                    M: {
                        "extraction": {
                            M: {
                                "status": {
                                    S: "in_progress"
                                },
                                "execution_start_time": {
                                    S: start_execution_time
                                }
                            }
                        },
                        "preconfig": {
                            M: {

                            }
                        },
                        "jobs": {
                            L: [

                            ]
                        }
                    }
                }
            },
            ReturnConsumedCapacity: 'TOTAL',
            TableName: dynamodb_ds_history_table
        }

        if (typeof eventData.type !== 'undefined' && eventData.type) {
            params.Item.type = {
                S: eventData.type
            }
        }
        if (typeof eventData.start_time !== 'undefined' && eventData.start_time) {
            params.Item.start_time = {
                S: String(eventData.start_time)
            }
        }
        if (typeof eventData.end_time !== 'undefined' && eventData.end_time) {
            params.Item.end_time = {
                S: String(eventData.end_time)
            }
        }
        console.log(params)
        return await dynamodb.putItem(params).promise()
    };

    static async saveDatasource(data, headerData) {
        let jobs = data.jobs || []
        let configs = data.config || []
        let postJobs = data.post_jobs || []
        let postconfig = this.getDataInDynamoDBFormat(data.post_steps)
        let preconfig = this.getDataInDynamoDBFormat(data.pre_steps)
        let jobsData = this.getDataInDynamoDBFormat(jobs)
        var filterConfig = configs
        configs = this.getDataInDynamoDBFormat(configs)
        let postjobs = this.getDataInDynamoDBFormat(postJobs)
        let environment = process.env.ENV === 'production' ? '' : `${process.env.ENV}_`
        let params = {
            Item: {
                name: {
                    S: data.name
                },
                ds_type: {
                    S: data.ds_type || ''
                },
                db_name: {
                    S: `${environment}${headerData.wsuiteid}_db`
                },
                table_name: {
                    S: data.wclient_id + "_" + data.table_name || 'sample_table'
                },
                delivery_stream_name: {
                    S: process.env.ENV + "_" + data.wclient_id + "_" + data.table_name + "_ds" || 'sample_table_ds'
                },
                wclient_id: {
                    S: String(data.wclient_id)
                },
                company_id: {
                    S: String(headerData.wsuiteid)
                },
                company_slug: {
                    S: headerData.partner
                },
                config_steps: postconfig,
                pre_config: preconfig,
                jobs: jobsData,
                post_jobs: postjobs,
                config: { S: JSON.stringify(data.config) },
                provision: { S: 'pending' },
                start_time: { S: String(data.start_time * 1000) },
                end_time: { S: String(data.end_time * 1000) }
                // end_time: {
                //  S: String(
                //    moment()
                //      .subtract(1, "months")
                //      .valueOf()
                //  )
                // }
            },
            ReturnConsumedCapacity: 'TOTAL',
            TableName: dynamodb_ds_table
        }
        if (filterConfig['filter_attr']) {
            for (var attr of filterConfig['filter_attr']) {
                params.Item[attr + '_attr'] = { S: filterConfig[attr] }
            }
        }
        // If datasource is S3 then keep in structure so we can query later
        if (data.ds_type === 's3' && data.ds_subtype === 'folder') {
            params.Item.config = configs
        }
        if (typeof data.schema != 'undefined' && data.schema) {
            params.Item.schema = {}
            params.Item.schema.S = data.schema
        }
        await dynamodb.putItem(params).promise()
        return Promise.resolve('success')
    };

    static async isDatasourceExist(ds_name, callback) {
        var queryparams = {
            Key: {
                name: {
                    S: ds_name
                }
            },
            TableName: dynamodb_ds_table
        }
        let data = await dynamodb.getItem(queryparams).promise()
        if (data.hasOwnProperty('Item')) {
            return Promise.resolve(true)
        }
        return Promise.resolve(false)
    };

    static getDataInDynamoDBFormat(data, callback) {
        if (Array.isArray(data)) {
            let returnData = this.processArrayInfo(data)
            return { L: returnData }
        } else if (data !== null && typeof data === 'object') {
            let returnData = this.processObjectInfo(data)
            return { M: returnData }
        } else {
            callback({ S: data })
        }
    }

    static processArrayInfo(arrayData) {
        let processedArray = []
        if (arrayData.length == 0) {
            return processedArray
        } else {
            arrayData.forEach(dataelement => {
                if (typeof dataelement == 'object') {
                    let returnData = this.processObjectInfo(dataelement)
                    processedArray.push({ M: returnData })
                } else if (Array.isArray(dataelement)) {
                    let returnData = this.processArrayInfo(dataelement)
                    processedArray.push({ L: returnData })
                } else {
                    processedArray.push({ S: String(dataelement) })
                }
            })
            return processedArray
        }
    }

    static processObjectInfo(objectData) {
        let finalObjectData = {}
        for (var k in objectData) {
            if (objectData.hasOwnProperty(k)) {
                if (Array.isArray(objectData[k])) {
                    let returnData = this.processArrayInfo(objectData[k])
                    finalObjectData[k] = { L: returnData }
                } else if (typeof objectData[k] == 'object') {
                    let returnData = this.processObjectInfo(objectData[k])
                    finalObjectData[k] = { M: returnData }
                } else {
                    finalObjectData[k] = { S: String(objectData[k]) }
                }
            }
        }
        return finalObjectData
    };
}
