var _ = require('lodash');

// module.exports = class execute {
module.exports = class CrudController {

    constructor() {}

    /**
     * get data from query
     * @param event get api request data
     */
    static GetDataList(event) {
        // Set DB credentials
        process.env['db'] = event.brand_config.mysqldbname
        process.env['host'] = event.brand_config.mysqlhost
        process.env['username'] = event.brand_config.mysqlusername
        process.env['password'] = event.brand_config.mysqlpassword

        console.log('event  ===>', event);
        var queryParam = event.queryParam || {};
        event.query = queryParam;

        var model = event.config.routeNode;
        var Resource = require('./../Models/' + model);
        console.log("Resource ========>", Resource);
        console.log('queryParam ----->', queryParam);
        return new Promise((resolve, reject) => {
            try {
                if (queryParam.resource_id !== null && queryParam.resource_id !== undefined) {
                    Resource.findOne({ id: queryParam.resource_id }, function(error, results) {
                        if (error) {
                            reject({ error: true, message: null, data: error });
                        } else {
                            resolve({ error: false, message: null, data: (results.data) ? JSON.parse(results.data) : [], total: (results.total) ? results.total : 0 });
                        }
                    });
                } else {
                    if (queryParam && queryParam !== null && queryParam !== undefined) {
                        Resource.find(queryParam, function(error, results) {
                            if (error) {
                                reject({ error: true, message: null, data: error });
                            } else {
                                resolve({ error: false, message: null, data: (results.data) ? JSON.parse(results.data) : [], total: (results.total) ? results.total : 0 });
                            }
                        });
                    } else {
                        Resource.find({}, function(error, results) {
                            if (error) {
                                reject({ error: true, message: null, data: error });
                            } else {
                                resolve({ error: false, message: null, data: (results.data) ? JSON.parse(results.data) : [], total: (results.total) ? results.total : 0 });
                            }
                        });
                    }
                }

            } catch (error) {
                console.log(error);
                reject({ error: true, message: error.message, data: null });
            }
        });
    }

    /**
     * get data based on primary key
     * @param event get api request data
     */
    static GetIdBaseData(event) {
        
        return new Promise((resolve, reject) => {
            try {
                var queryParam = event.queryParam || {};
                resolve({ error: false, message: null, data: queryParam });

            } catch (error) {
                console.log(error);
                reject({ error: true, message: error.message, data: null });
            }
        });
    }

    /**
     * Store Data
     * @param event get api request data
     */
    static AddData(event) {
        return new Promise((resolve, reject) => {
            try {
                process.env['db'] = event.brand_config.mysqldbname
                process.env['host'] = event.brand_config.mysqlhost
                process.env['username'] = event.brand_config.mysqlusername
                process.env['password'] = event.brand_config.mysqlpassword
                console.log('Add Data Event =============>', event);
                var model = event.config.routeNode;
                var Resource = require('./../Models/' + model);
                const formData = event.payload.requestData;

                console.log('formData -=-=-=-->', formData);
                if (!formData.resource && formData.resources == undefined) {
                    var resource = new Resource(formData);
                    resource.create(function(error, results) {
                        if (error) {
                            reject({ error: true, message: null, data: error });
                        } else {
                            resolve({ error: false, message: null, data: results });
                        }
                    });
                } else {
                    var resourcebatch = new Resource(formData.resource);
                    resourcebatch.createBatch(function(error, results) {
                        if (error) {
                            reject({ error: true, message: null, data: error });
                        } else {
                            resolve({ error: false, message: null, data: results });
                        }
                    });
                }
            } catch (error) {
                console.log(error);
                reject({ error: true, message: error.message, data: null });
            }
        });
    }


    /**
     * Store Data
     * @param event get api request data
     */
    static UpdateData(event) {
        return new Promise((resolve, reject) => {
            try {

                process.env['db'] = event.brand_config.mysqldbname
                process.env['host'] = event.brand_config.mysqlhost
                process.env['username'] = event.brand_config.mysqlusername
                process.env['password'] = event.brand_config.mysqlpassword

                var model = event.config.routeNode;
                var Resource = require('./../Models/' + model);
                var formData = event.payload.requestData;

                console.log('formData ---->', formData);
                //console.log('event.req.params.id ---->', event.req.params.id);
                //formData.id = event.req.params.id;
                var resource = new Resource(formData);
                resource.update(function(error, results) {
                    if (error) {
                        reject({ error: true, message: null, data: error });
                    } else {
                        resolve({ error: false, message: null, data: results });
                    }
                });
            } catch (error) {
                console.log(error);
                reject({ error: true, message: error.message, data: null });
            }
        });
    }

    /**
     * Delete Data
     * @param event get api request data
     */
    static DeleteData(event) {
        return new Promise((resolve, reject) => {
            try {
                
                process.env['db'] = event.brand_config.mysqldbname
                process.env['host'] = event.brand_config.mysqlhost
                process.env['username'] = event.brand_config.mysqlusername
                process.env['password'] = event.brand_config.mysqlpassword

                var queryParam = event.queryParam || {};
                var model = event.config.routeNode;
                var Resource = require('./../Models/' + model);
                const recId = event.req.params.id;
                if (recId !== null && recId !== undefined) {
                    var resource = new Resource({ id: recId });
                    resource.destroy(function(error, results) {
                        if (error) {
                            reject({ error: true, message: null, data: error });
                        } else {
                            resolve({ error: false, message: null, data: results });
                        }
                    });
                } else {
                    // var formData = JSON.parse(event.query);
                    var formData = event.payload.requestData;
                    var resource = new Resource(formData);
                    resource.destroy(function(error, result) {
                        if (error) {
                            reject({ error: true, message: null, data: error });
                        } else {
                            resolve({ error: false, message: null, data: results });
                        }
                    });
                }
            } catch (error) {
                console.log(error);
                reject({ error: true, message: error.message, data: null });
            }
        });
    }

}