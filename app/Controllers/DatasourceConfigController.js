module.exports = class DatasourceConfigController {
    constructor(){}

    static async checkconfig(event) {
        let eventData = event.queryParam
        let headerData = JSON.parse(event.headers.extraparams)
        let dataSource = require(`./ds-config/${eventData.type}/index`)
        try {
            let response = await dataSource(eventData, headerData)
            return Promise.resolve({ data: response, message: null, error: false })
        } catch (error) {
            if(error[0]) {
                return Promise.resolve({ data: null, message: error[0].message, error: true })    
            }
            return Promise.resolve({ data: null, message: error, error: true })
        }
    }
}