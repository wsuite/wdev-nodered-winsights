const AWS = require('aws-sdk');
const dynamodb = new AWS.DynamoDB();

module.exports = class DynamoDBController {
    constructor(){}

    static async getdata(event) {
        let eventData = event.queryParam
        let response = await this.getDynamodbData(eventData)
        return Promise.resolve({ error: false, data: response, message: 'Got data'})
    }

    static async getDynamodbData(eventData, key = null) {
        let TableName = eventData.tbl_name;
        let AttributeName = eventData.attribute_name;
        let AttributeHasName = "#"+AttributeName;
        let FilterExpression = "#"+AttributeName+" = :n";
        let AttributeValue = eventData.attribute_value;
        
        
        if(eventData.primary) {
            var params = {
                TableName: TableName,
                KeyConditionExpression: FilterExpression,
                ExpressionAttributeValues: {
                  ":n": {
                    S: AttributeValue
                  }
                },
                ExpressionAttributeNames: {
                    [AttributeHasName]: AttributeName
                }
              };
    
            if(key) {
                params['ExclusiveStartKey'] = key
            }
            return await dynamodb.query(params).promise()
        } else {
            var params = {
                TableName: TableName,
                FilterExpression: FilterExpression,
                ExpressionAttributeValues: {
                  ":n": {
                    S: AttributeValue
                  }
                },
                ExpressionAttributeNames: {
                    [AttributeHasName]: AttributeName
                }
              };
    
            if(key) {
                params['ExclusiveStartKey'] = key
            }

            let res = await dynamodb.scan(params).promise()
            if(typeof res.LastEvaluatedKey !== 'undefined' && res.Items.length === 0) {
                return await getDynamodbData(eventData,res.LastEvaluatedKey)
            }
            return res
        }
    }
}