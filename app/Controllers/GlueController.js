var AWS = require("aws-sdk");
var glue = new AWS.Glue();
var s3 = new AWS.S3();
var cloudwatchlogs = new AWS.CloudWatchLogs();

module.exports = class GlueController {

    constructor() { }

    /**
     * Run Glue Job
     * @param {*} event 
     */
    static async runjob(event) {
        try {
            if (event.payload.requestData && event.payload.requestData.job_name) {
                let jobName = event.payload.requestData.job_name
                var params = {
                    JobName: jobName
                };
                let response = await glue.startJobRun(params).promise()
                return { error: false, data: { run_id: response.JobRunId } }
            } else {
                return { error: true, data: null, message: 'Missing required parameters', data: null }
            }
        } catch (error) {
            if (error.code === 'EntityNotFoundException') {
                return { error: true, data: null, message: 'Job not found', data: null }
            }
            return { error: true, data: null, message: error, data: null }
        }
    }

    static async updatejob(event) {
        try {
            if (event.payload.requestData && event.payload.requestData.job_name) {
                let jobRes = await glue.getJob({ JobName: event.payload.requestData.job_name }).promise()
                if (jobRes) {
                    let jobData = jobRes.Job
                    let eventData = event.payload.requestData
                    let ext = "";
                    if (eventData.language == "python") {
                        ext = ".py";
                    } else if (eventData.language == "scala") {
                        ext = ".scala";
                    }

                    const bucketName = 'winsights-data';
                    let key = `${eventData.wsuite_id}/winsights/job_scripts/${eventData.analysis_id}/${eventData.job_name}${ext}`;

                    const params = {
                        Bucket: bucketName,
                        Key: key,
                        Body: eventData.code
                    };
                    await s3.upload(params).promise()
                    // Update glue job
                    let paramsGlue = {
                        JobName: eventData.job_name, /* required */
                        JobUpdate: { /* required */
                            AllocatedCapacity: eventData.capacity || 2,
                            Command: {
                                Name: 'glueetl',
                                ScriptLocation: `s3://${bucketName}/${key}`,
                                PythonVersion: 3
                            },
                            DefaultArguments: {
                                '--job-language': eventData.language
                            },
                            Role: 'arn:aws:iam::133013689155:role/AWSGlueServiceRole-insights'
                        }
                    };
                    if (jobData.Connections) {
                        paramsGlue['JobUpdate']['Connections'] = jobData.Connections
                    }
                    if (jobData.DefaultArguments) {
                        paramsGlue['JobUpdate']['DefaultArguments'] = jobData.DefaultArguments
                    }
                    if (jobData.GlueVersion) {
                        paramsGlue['JobUpdate']['GlueVersion'] = jobData.GlueVersion
                    }
                    if (jobData.Command.PythonVersion) {
                        paramsGlue['JobUpdate']['Command']['PythonVersion'] = jobData.Command.PythonVersion
                    }
                    await glue.updateJob(paramsGlue).promise()
                    return { error: false, data: {}, message: 'Job is updated' }
                }
                return { error: false, data: { run_id: response.JobRunId } }
            } else {
                return { error: true, data: null, message: 'Missing required parameters', data: null }
            }
        } catch (error) {
            if (error.code === 'EntityNotFoundException') {
                return { error: true, data: null, message: 'Job not found', data: null }
            }
            return { error: true, data: null, message: error, data: null }
        }
    }

    /**
     * Get job status
     * @param {*} event 
     */
    static async getstatus(event) {
        try {
            if (event.payload.requestData && event.payload.requestData.job_name && event.payload.requestData.job_run_id) {
                let jobName = event.payload.requestData.job_name
                let jobRunId = event.payload.requestData.job_run_id
                var params = {
                    JobName: jobName,
                    RunId: jobRunId
                };
                let response = await glue.getJobRun(params).promise()
                if (response.JobRun.JobRunState == "SUCCEEDED") {
                    var params = {
                        logGroupName: '/aws-glue/jobs/error', /* required */
                        logStreamName: jobRunId, /* required */
                    };
                    let data = await cloudwatchlogs.getLogEvents(params).promise()
                    let rows = data.events;
                    let str = "";
                    rows.forEach(function (element, rowKey) {
                        let ele = element.message;
                        let test = ele.replace(/\n/g, "");
                        let firstvariable = "Log Contents:"; //first input;
                        let secondvariable = "End of LogType"; //second input;
                        let regExString = new RegExp("(?:" + firstvariable + ")(.*?)(?:" + secondvariable + ")", "ig");
                        let testRE = regExString.exec(test);
                        if (testRE && testRE.length > 1 && testRE != null) {
                            str += testRE[1] + "\n";
                        }
                    })
                    response.JobRun.JobResult = str;
                }
                return { error: false, data: response }
            } else {
                return { error: true, data: null, message: 'Missing required parameters', data: null }
            }
        } catch (error) {
            console.log(error)
            if (error.code === 'EntityNotFoundException') {
                return { error: true, data: null, message: 'Job not found', data: null }
            }
            return { error: true, data: null, message: error, data: null }
        }
    }

    /**
     * Delete glue job
     * @param {*} event 
     */
    static async deletejob(event) {
        try {
            if (event.payload.requestData && event.payload.requestData.job_name) {
                let jobName = event.payload.requestData.job_name
                var params = {
                    JobName: jobName
                };
                await glue.deleteJob(params).promise()
                return { error: false, data: {}, message: 'Job is deleted' }
            } else {
                return { error: true, data: null, message: 'Missing required parameters', data: null }
            }
        } catch (error) {
            if (error.code === 'EntityNotFoundException') {
                return { error: true, data: null, message: 'Job not found', data: null }
            }
            return { error: true, data: null, message: error, data: null }
        }
    }

    /**
     * Stop glue job
     * @param {*} event 
     */
    static async stopjob(event) {
        try {
            if (event.payload.requestData && event.payload.requestData.job_name && event.payload.requestData.job_run_id) {
                let jobName = event.payload.requestData.job_name
                let jobRunId = event.payload.requestData.job_run_id
                var params = {
                    JobName: jobName,
                    JobRunIds: [
                        jobRunId
                    ]
                };
                await glue.batchStopJobRun(params).promise()
                return { error: false, data: {}, message: 'Stopping job' }
            } else {
                return { error: true, data: null, message: 'Missing required parameters', data: null }
            }
        } catch (error) {
            if (error.code === 'EntityNotFoundException') {
                return { error: true, data: null, message: 'Job not found', data: null }
            }
            return { error: true, data: null, message: error, data: null }
        }
    }

    /**
     * Create Glue Job
     * @param {*} event 
     */
    static async createJob(event) {
        try {
            if (event.payload.requestData) {
                let eventData = event.payload.requestData
                let headerData = JSON.parse(event.headers.extraparams)
                let ext = "";
                if (eventData.language == "python") {
                    ext = ".py";
                } else if (eventData.language == "scala") {
                    ext = ".scala";
                }

                const bucketName = 'winsights-data';
                const key = `${eventData.wsuite_id}/winsights/job_scripts/${eventData.analysis_id}/${eventData.name}${ext}`;
                console.log(key)
                const params = {
                    Bucket: bucketName,
                    Key: key,
                    Body: eventData.code
                };
                await s3.upload(params).promise()
                var glueparams = {
                    Command: {
                        Name: 'glueetl',
                        ScriptLocation: `s3://${bucketName}/${key}`
                    },
                    Name: eventData.name,
                    DefaultArguments: {
                        '--job-language': eventData.language
                    },
                    Role: 'arn:aws:iam::133013689155:role/AWSGlueServiceRole-insights',
                    AllocatedCapacity: eventData.capacity || 2,
                    ExecutionProperty: {
                        MaxConcurrentRuns: 500
                    },
                    Tags: {
                        CompanyID: headerData.wsuiteid,
                        Environment: process.env.ENV
                    }
                };
                await glue.createJob(glueparams).promise()
                return { error: false, message: 'Glue job saved successfully', data: {} }
            } else {
                return { error: true, data: null, message: 'Missing required parameters', data: null }
            }
        } catch (error) {
            if (error.code === 'EntityNotFoundException') {
                return { error: true, data: null, message: 'Job not found', data: null }
            }
            return { error: true, data: null, message: error, data: null }
        }
    }
}

