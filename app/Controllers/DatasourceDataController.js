module.exports = class DatasourceDataController {
    constructor() {}

    static async getData(event) {
        return new Promise(function(resolve, reject) {
            // executor (the producing code, "singer")
            let eventData = event.payload.requestData;
            getDataFromDatasource(eventData, function(error, data) {
                console.log("getDataFromDatasource ===>", error, data);
                if (error) {
                    // callback(JSON.stringify(error));
                    resolve({ data: 'no', message: JSON.stringify(error), error: true })
                } else {
                    // callback(null, data)
                    resolve({ data: data, message: '', error: false })
                }
            });
        });
    }
}


var getDataFromDatasource = function(eventData, callback) {
    console.log('String');
    if (typeof eventData.ds_type == 'undefined' || !eventData.ds_type) {
        return callback('type is not defined')
    }
    var datasource = require('./ds-data/' + eventData.ds_type + '/resource')
    datasource.fetchData(eventData, function(error, data) {

        console.log("fetchData --->", error, data);
        if (error) {
            const errResponse = {
                statusCode: 404,
                body: { 'message': error, 'data': null },
            };
            callback(errResponse, null)
        } else {
            const response = {
                statusCode: 200,
                body: { 'message': 'Found data', 'data': data },
            };
            callback(null, response)
        }
    })
}