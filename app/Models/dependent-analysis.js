var myModel = require('../Config/dbConfig')

var Resource = myModel.model('dependent-analysis', {
  table: 'wsuite_winsights_dependent_analysis',
  fields: ['id', 'analysis_id', 'dependent_analysis_id', 'status', 'execute_on_error'],
  primary: ['id']
});

module.exports = Resource