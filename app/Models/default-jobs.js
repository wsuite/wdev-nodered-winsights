var myModel = require('../Config/dbConfig')

var Resource = myModel.model('default-jobs', {
  table: 'wsuite_winsights_default_jobs',
  fields: ['id','name', 'job_type','language','code'],
  primary: ['id']
});

module.exports = Resource