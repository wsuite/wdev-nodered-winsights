var myModel = require('../Config/dbConfig')

var Resource = myModel.model('client-config', {
  table: 'wsuite_winsights_client_config',
  fields: ['client_id', 'access_key_id', 'secret_access_key'],
  primary: ['client_id']
});

module.exports = Resource