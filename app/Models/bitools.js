var myModel = require('../Config/dbConfig')

var Resource = myModel.model('bitools', {
  table: 'wsuite_winsights_bitools',
  fields: ['id', 'name', 'description', 'image','created_at'],
  primary: ['id']
});

module.exports = Resource