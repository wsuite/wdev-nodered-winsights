var myModel = require('../Config/dbConfig')

var Resource = myModel.model('jobdata', {
  table: 'wsuite_winsights_jobdata',
  fields: ['id', 'name', 'job_type', 'language', 'code', 'status', 'client_id', 'jobs_id', 'run_id', 'capacity', 'query_execution_id', 'response', 'order', 'created_at', 'updated_at', 'is_deleted'],
  primary: ['id']
});

module.exports = Resource