var myModel = require('../Config/dbConfig')

var Resource = myModel.model('saved-jobs', {
  table: 'wsuite_winsights_saved_jobs',
  fields: ['id', 'name', 'job_type', 'language', 'code', 'status', 'jobs_id','created_at', 'updated_at'],
  primary: ['id']
});

module.exports = Resource