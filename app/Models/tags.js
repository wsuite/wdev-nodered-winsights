var tagModel = require('../Config/dbConfig')

var Tags = tagModel.model('tags', {
  table: 'wsuite_winsights_tags',
  fields: ['id', 'tags'],
  primary: ['id']
});

module.exports = Tags