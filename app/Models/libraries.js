var myModel = require('../Config/dbConfig')

var Resource = myModel.model('libraries', {
  table: 'wsuite_winsights_libraries',
  fields: ['id', 'name', 'runtime', 'code', 'handler','is_deleted'],
  primary: ['id']
});

module.exports = Resource