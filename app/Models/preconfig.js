var myModel = require('../Config/dbConfig')

var Resource = myModel.model('preconfig', {
  table: 'wsuite_winsights_preconfig',
  fields: ['id', 'name', 'runtime', 'description', 'code', 'analysis_id','handler','from'],
  primary: ['id']
});

module.exports = Resource
