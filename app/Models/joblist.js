var myModel = require('../Config/dbConfig')

var Resource = myModel.model('joblist', {
  table: 'wsuite_winsights_joblist',
  fields: ['id', 'name', 'job_type', 'language', 'code', 'status', 'client_id', 'order', 'created_at', 'updated_at', 'is_deleted', 'run_id', 'response', 'capacity'],
  primary: ['id']
});

module.exports = Resource