var myModel = require('../Config/dbConfig')

var Resource = myModel.model('tables', {
  table: 'wsuite_winsights_tables',
  fields: ['id', 
           'name', 
           'column_count', 
           'size', 
           'created_at', 
           'updated_at',
          'clientid'],
  primary: ['id']
});

module.exports = Resource