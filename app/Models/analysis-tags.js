var tagModel = require('../Config/dbConfig')

var Tags = tagModel.model('analysis-tags', {
  table: 'wsuite_winsights_analysis_tags',
  fields: ['id','analysis_id', 'tags_id'],
  primary: ['id']
});

module.exports = Tags