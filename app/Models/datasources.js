var myModel = require('../Config/dbConfig')

var Resource = myModel.model('datasources', {
  table: 'wsuite_winsights_datasources',
  fields: ['id', 'analysis_id', 'datasource_type_id', 'config', 'data_sample', 'created_at', 'updated_at','is_deleted','sample_fetched','process_started'],
  primary: ['id']
});

module.exports = Resource