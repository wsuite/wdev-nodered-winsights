var myModel = require('../Config/dbConfig')

var Resource = myModel.model('jobs', {
  table: 'wsuite_winsights_jobs',
  fields: ['id', 
           'name', 
           'type', 
           'description', 
           'period', 
           'period_value',
           'status',
           'analysis_id',
           'created_at', 
           'updated_at'],
  primary: ['id']
});

module.exports = Resource