var tagModel = require('../Config/dbConfig')

var Tags = tagModel.model('job-tags', {
  table: 'wsuite_winsights_job_tags',
  fields: ['id','jobs_id', 'tags_id'],
  primary: ['id']
});

module.exports = Tags