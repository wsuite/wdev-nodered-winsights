//  var connection = require('../core/dbconfig')

// var Resource = connection.extend({
//     tableName: "wsuite_winsights_etl_process",
// });

// module.exports = Resource

var myModel = require('../Config/dbConfig')

var Resource = myModel.model('analysis', {
    table: 'wsuite_winsights_analysis',
    fields: ['id', 'client_id', 'name', 'description', 'type', 'period', 'status', 'shared_with', 'last_run_time', 'lock_time', 'current_step_id', 'created_by', 'last_run_by', 'created_at', 'updated_at', 'tags', 'is_deleted', 'period_value'],
    primary: ['id']
});

module.exports = Resource