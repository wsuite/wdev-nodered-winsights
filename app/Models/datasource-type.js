var myModel = require('../Config/dbConfig')

var Resource = myModel.model('datasource-type', {
  table: 'wsuite_winsights_datasource_type',
  fields: ['id', 'name', 'slug', 'image', 'is_enable', 'is_available','is_deleted'],
  primary: ['id']
});

module.exports = Resource