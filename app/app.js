module.exports = {
    GlueController: require('./Controllers/GlueController'),
    ExecutionHistoryController: require('./Controllers/ExecutionHistoryController'),
    DynamoDBController: require('./Controllers/DynamoDBController'),
    AthenaController: require('./Controllers/AthenaController'),
    DatasourceController: require('./Controllers/DatasourceController'),
    CrudController: require('./Controllers/CrudController'),
    DatasourceConfigController: require('./Controllers/DatasourceConfigController'),
    DatasourceDataController: require('./Controllers/DatasourceDataController')
}