module.exports = async(event) => {
    const { GlueController, ExecutionHistoryController, DynamoDBController, AthenaController, CrudController, DatasourceController, DatasourceConfigController, DatasourceDataController } = require('./app')

    try {
        const method = (event.headers && event.headers.httpmethod) ? event.headers.httpmethod.toUpperCase() : event.req.method.toUpperCase();
        console.log('method --->', method);

        if (event.config.routeNode === 'handlejob' && event.payload.requestData.action === 'run') {
            return await GlueController.runjob(event)
        } else if (event.config.routeNode === 'handlejob' && event.payload.requestData.action === 'stop') {
            return await GlueController.stopjob(event)
        } else if (event.config.routeNode === 'handlejob' && event.payload.requestData.action === 'delete') {
            return await GlueController.deletejob(event)
        } else if (event.config.routeNode === 'handlejob' && event.payload.requestData.action === 'status') {
            return await GlueController.getstatus(event)
        } else if (event.config.routeNode === 'handlejob' && event.payload.requestData.action === 'update') {
            return await GlueController.updatejob(event)
        } else if (event.config.routeNode === 'athena' && event.queryParam.action === 'execute') {
            return await AthenaController.runquery(event)
        } else if (event.config.routeNode === 'athena' && event.queryParam.action === 'status') {
            return await AthenaController.status(event)
        } else if (event.config.routeNode === 'athena' && event.queryParam.action === 'result') {
            return await AthenaController.result(event)
        } else if (event.config.routeNode === 'athena' && event.payload.requestData.action === 'stop') {
            return await AthenaController.stop(event)
        } else if (event.config.routeNode === 'athena' && event.queryParam.action === 'truncate') {
            return await AthenaController.truncate(event)
        } else if (event.config.routeNode === 'athena' && event.payload.requestData.action === 'download') {
            return await AthenaController.download(event)
        } else if (event.config.routeNode === 'executionhistory') {
            return await ExecutionHistoryController.getdata(event)
        } else if (event.config.routeNode === 'dynamodb') {
            return await DynamoDBController.getdata(event)
        } else if (event.config.routeNode === 'createds') {
            return await DatasourceController.create(event)
        } else if (event.config.routeNode === 'runanalysis') {
            return await DatasourceController.run(event)
        } else if (event.config.routeNode === 'handlejob') {
            return await GlueController.createJob(event)
        } else if (event.config.routeNode === 'dsconfig') {
            return await DatasourceConfigController.checkconfig(event)
        } else if (event.config.routeNode === 'dataprovider') {
            return await DatasourceDataController.getData(event)

            // ----- CRUD Node - START -----
        } else if (method === 'GET' && event.config.routeNode != '') {
            return await CrudController.GetDataList(event);
        } else if (method === 'POST' && event.config.routeNode != '') {
            return await CrudController.AddData(event);
        } else if (method === 'PUT' && event.config.routeNode != '') {
            return await CrudController.UpdateData(event);
        } else if (method === 'DELETE' && event.config.routeNode != '') {
            return await CrudController.DeleteData(event);
            // ----- CRUD Node - END -----

        } else {
            console.log('error 66666', event.headers);
            return { error: true, message: `Not found ${event.config.routeNode} node` };
        }

    } catch (error) {
        console.log(error)
        return { error: true, message: `Error while running api ${error}` };
    }

}