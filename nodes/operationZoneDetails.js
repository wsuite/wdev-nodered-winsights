const app = require("../core/app");
const creator = require('../core/queryCreator');

module.exports = function (RED) {
  function operationZoneDetailsQuery(config) {
    RED.nodes.createNode(this, config);
    var node = this;
    node.on("input", function (msg) {
      let payload = msg.payload;
      let headers = {
        query: payload
      };
      const App = new app(config.accessKeyId, config.secretAccessKey, config.database);
      let query = creator(msg, config, headers);
      msg.Payload ? msg.Payload.query = query : msg.payload.query = query;
      App.getTableData(msg)
        .then(res => {
          const result = {
            success: true,
            response: res
          };
          msg.payload = result;
          node.send(msg);
        })
        .catch(err => {
          console.log("Error", err);
          msg.statusCode = err.code;
          res = { success: false, error_message: err.message };
          node.send(msg);
        });
    });
  }
  RED.nodes.registerType("operationZoneDetails", operationZoneDetailsQuery);
};
