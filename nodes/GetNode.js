var customRoute = require('../app/route')

module.exports = function(RED) {
    function GetNodeData(config) {

        RED.nodes.createNode(this, config);
        var node = this;
        node.on('input', function(msg) {
            // --------------------------
            msg.config = config;
            msg.payload = {
                requestData: msg.payload
            };
            msg.queryParam = msg.req.query;
            // console.log("msg", msg);
            customRoute(msg).then((res, err) => {
                console.log("In GET Node err", err);
                // console.log("In GET Node res", res);

                if (res.error) {
                    msg.statusCode = 400;
                } else {
                    msg.statusCode = 200;
                }
                msg.payload = res;
            }).catch((err) => {

                console.log("In GETNODE-catch ", err);
                msg.payload = err;
                msg.statusCode = 400;
            }).finally(() => {
                // console.log("In GETNODE-final", msg);
                node.send(msg);
            });

            // --------------------------

        });
    }
    RED.nodes.registerType("GetNode", GetNodeData);
}