const app = require("../core/app");
const mySqlUtility = require("../core/mysql-utility/mysql-utility");
const creator = require('../core/queryCreator');

module.exports = function (RED) {
  function MiTrabajoChartQuery(config) {
    RED.nodes.createNode(this, config);
    var node = this;
    node.on("input", function (msg) {
      let payload = msg.payload;
      let headers = {
        query: payload
      };
      const App = new app(config.accessKeyId, config.secretAccessKey, config.database);
      let query = creator(msg, config, headers);
      msg.Payload ? msg.Payload.query = query : msg.payload.query = query;
      if (config.isathena == '' || config.isathena == false) {
        const connection = mySqlUtility.dbConnection(config);
        const data = mySqlUtility.getRecords(connection, query);
        data.then(res => {
          msg.payload = res;
          node.send(msg);
        }).catch(err => {
          msg.statusCode = err.code;
          msg.payload = { success: false, error_message: err.msg };
          node.send(msg);
        }).finally(() => connection.destroy());
      } else {
        App.getTableData(msg)
          .then(res => {
            const result = {
              success: true,
              response: res
            };
            msg.payload = result;
            node.send(msg);
          })
          .catch(err => {
            msg.statusCode = err.code;
            res = { success: false, error_message: err.message };
            node.send(msg);
          });
      }
    });
  }
  RED.nodes.registerType("mitrabajochart", MiTrabajoChartQuery);
};
