const app = require("../core/app");
module.exports = function(RED) {
  function AthenaQuery(config) {
    RED.nodes.createNode(this, config);
    var node = this;
    node.on("input", function(msg) {
      const App = new app(config.accessKeyId, config.secretAccessKey, config.database);
      App.getTableData(msg)
        .then(res => {
          const result = {
            success: true,
            response: res
          };
          msg.payload = result;
          node.send(msg);
        })
        .catch(err => {
          console.log("Error", err);
          msg.statusCode = err.code;
          res = { success: false, error_message: err.message };
          node.send(msg);
        });
    });
  }
  RED.nodes.registerType("athena", AthenaQuery);
};
