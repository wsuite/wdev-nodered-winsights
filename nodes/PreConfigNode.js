var customRoute = require('../app/route')

module.exports = function(RED) {
    function PostNodeData(config) {

        RED.nodes.createNode(this, config);
        var node = this;
        node.on('input', function(msg) {
            msg.statusCode = 200;
            node.send(msg);
        });
    }
    RED.nodes.registerType("PreConfigNode", PostNodeData);
}