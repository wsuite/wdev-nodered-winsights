//var execute = require('../core/app');
//var lambda_function = require('../lambda/index');

module.exports = function (RED) {
  function GetFeeReportNode(config) {
    RED.nodes.createNode(this, config);
    var node = this;

    node.on('input', function (msg) {
      console.log("msg.req.params", msg.req.params);
      //=== START: SET STATIC DATA ===//
      result = {
        "fee":{
          "resources_list":[
            {
              "resource":"Diseñador UX",
              "plan":"50% (80h)",
              "executed":"25% (40h)", 
              "status": "up"
            },
            {
              "resource":"Diseñador UI",
              "plan":"50% (80h)",
              "executed":"25% (40h)", 
              "status": "up"
            },
            {
              "resource":"PM",
              "plan":"50% (80h)",
              "executed":"25% (40h)", 
              "status": "up"
            },
            {
              "resource":"Desarrollador",
              "plan":"50% (80h)",
              "executed":"100% (88h)", 
              "status": "down"
            },
          ],
          "hours":{
            "plan":320,
            "executed":160,
            "status":"up"
          },
          "resurces":{
            "plan":12,
            "executed":6,
            "status":"up"
          },
          "cop":{
            "plan":"$1.300.000",
            "executed":"Monthly",
            "status":"down"
          }
        },
        "report":{
          "resources_list":[
            {
              "resource":"Diseñador UX",
              "plan":"50% (80h)",
              "executed":"25% (40h)", 
              "status": "up"
            },
            {
              "resource":"Diseñador UI",
              "plan":"50% (80h)",
              "executed":"25% (40h)", 
              "status": "up"
            },
            {
              "resource":"PM",
              "plan":"50% (80h)",
              "executed":"25% (40h)", 
              "status": "down"
            },
            {
              "resource":"Desarrollador",
              "plan":"50% (80h)",
              "executed":"100% (88h)", 
              "status": "up"
            },
          ],
          "hours":{
            "plan":320,
            "executed":160,
            "status":"down"
          },
          "resurces":{
            "plan":12,
            "executed":6,
            "status":"down"
          },
          "cop":{
            "plan":"$1.300.000",
            "executed":"Monthly",
            "status":"up"
          }
        },
      };
      msg.payload = {
        data :  result,
        message : "",                    
      };                 
      node.send(msg);
      //=== END: SET STATIC DATA ===//

      //=== START: GET DATA USING ACTUAL LAMBDA FUNCTION ===//
      /*
      var token = null;
      if (msg.req.headers["session_id"]) {
        token = msg.req.headers["session_id"];
        console.log(token);
      }
      var table = config.table;
      var method = config.method;    
      var event = {
        "Payload": msg.payload,
        "Execution": {
          "Name": "UniqueKey",                
        },
        "Config": {
          "UniqueKey": {
            "name": "UniqueName",
            "table": table,
            "method": method
          }
        },
        "headers": {
          "authorizetoken": "",
          "host": "",
          "httpmethod": msg.req.method,
          "params": msg.req.params,
          "query": msg.req.query
        }
      }

      lambda_function.handler(event, null)
      .then(function (result) {
        console.log(JSON.stringify(result));
        msg.payload = {
          data: result,
          message: "",
        };
        node.send(msg);        
      })
      .catch(function(error){
        console.log('result index')        
          console.log(JSON.stringify(error));
          msg.payload = {
            data: "",
            message: error.message,
          };
          node.send(msg);        
      });
      */
      //=== END: GET DATA USING ACTUAL LAMBDA FUNCTION ===//
    });
  }
  RED.nodes.registerType("getFeeReport", GetFeeReportNode);
}