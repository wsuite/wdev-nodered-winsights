const mySqlUtility = require("../core/mysql-utility/mysql-utility");
const creator = require("../core/myChartQuery");

module.exports = function(RED) {
	function MyChartQuery(config) {
		RED.nodes.createNode(this, config);
		var node = this;
		node.on("input", function(msg) {
			let payload = msg.payload;
			let headers = {
				query: payload
			};
			let query = creator(headers);
			msg.Payload ? (msg.Payload.query = query) : (msg.payload.query = query);
			const connection = mySqlUtility.dbConnection(config);
			const data = mySqlUtility.getRecords(connection, query);
			data
				.then(res => {
					msg.payload = res;
					node.send(msg);
				})
				.catch(err => {
					msg.statusCode = err.code;
					msg.payload = { success: false, error_message: err.msg };
					node.send(msg);
				})
				.finally(() => connection.destroy());
		});
	}
	RED.nodes.registerType("mychart", MyChartQuery);
};
