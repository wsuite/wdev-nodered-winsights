var customRoute = require('../app/route')

module.exports = function(RED) {
    function WIDeleteNodeData(config) {

        RED.nodes.createNode(this, config);
        var node = this;
        node.on('input', function(msg) {

            // --------------------------

            msg.config = config;
            msg.payload = {
                requestData: msg.payload
            };
            msg.queryParam = msg.req.query;

            // console.log("msg", msg);
            // console.log("==========================");
            // console.log("msg.payload", msg.payload);

            customRoute(msg).then((res, err) => {
                // console.log("In Node res", res);
                console.log("In Node err", err);

                if (res.error) {
                    msg.statusCode = 400;
                } else {
                    msg.statusCode = 200;
                }
                msg.payload = res;
            }).catch((err) => {

                console.log("In POSTNODE-catch ", err);
                msg.payload = err;
                msg.statusCode = 400;
            }).finally(() => {
                // console.log("In POSTNODE-final", msg);
                node.send(msg);
            });

            // --------------------------

        });
    }
    RED.nodes.registerType("WIDeleteNode", WIDeleteNodeData);
}